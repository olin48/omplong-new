<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="row">
        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:50px;">
                    <div class="news-widget">
                        <i class="fontello-calendar-1"></i>
                        <?php foreach ($harian as $har) : ?>
                            <h4 class="text-blue"><?= $har['total_data_harian']; ?></h4>
                        <?php endforeach ?>
                        <h5>DATA HARIAN</h5>
                        <div style="clear:both;"></div>
                    </div>
                    <a class="small-box-footer" href="input_data">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:50px;">
                    <div class="news-widget">
                        <i class="fontello-clipboard"></i>
                        <?php foreach ($st_pengamatan as $sp) : ?>
                            <h4 class="text-green"><?= $sp['total_st_pengamatan']; ?></h4>
                        <?php endforeach ?>
                        <h5>STASIUN PENGAMATAN</h5>
                        <div style="clear:both;"></div>
                    </div>
                    <a class="small-box-footer" href="stasiun_pengamatan">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:50px;">
                    <div class="news-widget">
                        <i class="fontello-user-1"></i>
                        <?php foreach ($petani as $pet) : ?>
                            <h4 class="text-aqua"><?= $pet['total_data_petani']; ?></h4>
                        <?php endforeach ?>
                        <h5>PETANI</h5>
                        <div style="clear:both;"></div>
                    </div>
                    <a class="small-box-footer" href="stasiun_pengamatan">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:25px;">
                    <label style="font-size: 18px;">Grafik Harian</label>
                    <hr />
                    <center><b>Grafik Petani</b></center>
                    <canvas id="grafik_harian"></canvas>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

        <div class="col-lg-4">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:25px;">
                    <label style="font-size: 18px;">Data Harian Terakhir</label>
                    <hr />
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Tanggal</th>
                                <th>Jml CH</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($data_harian as $dh) : ?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><?= $dh['first_name']; ?></td>
                                    <td><?= $dh['tanggal']; ?></td>
                                    <td><?= $dh['jumlah_ch']; ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->