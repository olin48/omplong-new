<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data <?php echo $menu_title; ?></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#grafik_bulanan_tahunan">Grafik Bulanan & Tahunan</a></li>
                        <li><a data-toggle="tab" href="#grafik_box_plot">Grafik Box Plot</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="grafik_bulanan_tahunan" class="tab-pane fade in active">
                            <br />
                            <?= $this->session->flashdata('message'); ?>
                            <div class="form-group">
                                <label for="usr">Stasiun Pengamatan :</label>
                                <select name="id_sawah" id="id_sawah" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Stasiun Pengamatan</option>
                                    <?php foreach ($stasiun as $s) : ?>
                                        <option value="<?= $s['id']; ?>"><?= $s['kd_stasiun']; ?> - <?= $s['first_name']; ?> <?= $s['last_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="usr">Tahun :</label>
                                <select name="tahun" id="tahun" class="form-control" required>
                                    <option value="">Pilih...</option>
                                </select>
                            </div>
                            <br />
                            <button type="button" id="tampilkan_data_grafik" class="btn btn-success">Tampilkan <?php echo $menu_title; ?></button>
                        </div>
                        <div id="grafik_box_plot" class="tab-pane fade">
                            <br />
                            <?= $this->session->flashdata('message'); ?>
                            <div class="form-group">
                                <label for="usr">Stasiun Pengamatan :</label>
                                <select name="id_sawah_box" id="id_sawah_box" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Stasiun Pengamatan</option>
                                    <?php foreach ($stasiun as $s) : ?>
                                        <option value="<?= $s['id']; ?>"><?= $s['kd_stasiun']; ?> - <?= $s['first_name']; ?> <?= $s['last_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <br />
                            <button type="button" id="tampilkan_data_box_plot" class="btn btn-success">Tampilkan <?php echo $menu_title; ?></button>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div id="dataGrafik" class="col-lg-12" hidden>
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive col-lg-12" style="background: #ffffff">
                    <table class="table table-bordered">
                        <thead>
                            <tr id="headBulan">
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="bodyDasarianSatu">
                            </tr>
                            <tr id="bodyDasarianDua">
                            </tr>
                            <tr id="bodyDasarianTiga">
                            </tr>
                            <tr id="bodyDasarianTotal">
                            </tr>
                        </tbody>
                    </table>
                    <hr />
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#bulanan">Grafik Bulanan</a></li>
                        <li><a data-toggle="tab" href="#tahunan">Grafik Tahunan</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="bulanan" class="tab-pane fade in active">
                            <div class="row" style="padding-top: 20px;" id="grafik_ch_bulanan">
                            </div>
                        </div>
                        <div id="tahunan" class="tab-pane fade">
                            <div class="row" style="padding-top: 20px;" id="grafik_ch_tahunan">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div id="dataBoxPlot" class="col-lg-12" hidden>
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i></span>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive col-lg-12" style="background: #ffffff">
                    <h3 style="text-align: center;"><strong>Grafik Rentang Normal Curah Hujan Bulanan <span id="st_peng"></span></strong></h3>
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                    <hr />
                    <br />
                    <label>
                        <h4>Data Table</h4>
                    </label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Data Table</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>Mei</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Aug</th>
                                <th>Sep</th>
                                <th>Okt</th>
                                <th>Nov</th>
                                <th>Des</th>
                            </tr>
                        </thead>
                        <tbody id="bodyBoxPlot">
                        </tbody>
                    </table>
                    <hr />
                    <br />
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->