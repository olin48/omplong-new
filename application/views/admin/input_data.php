<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">

                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span id="menu_title"></span>
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <ul class="nav nav-tabs">
                        <li <?php if ($tab == "harian" || $tab == null) {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#harian">Data Harian</a></li>
                        <li <?php if ($tab == "musiman") {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#musiman">Data Musiman</a></li>
                        <li <?php if ($tab == "akhirmusim") {
                                echo "class='active'";
                            } ?>><a data-toggle="tab" href="#akhirmusim">Data Akhir Musim</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="harian" class="tab-pane fade <?php if ($tab == "harian" || $tab == null) {
                                                                    echo "in active";
                                                                } ?>">
                            <br />
                            <?= $this->session->flashdata('message'); ?>
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#data-harian">Buat Data <?php echo $menu_title; ?></button>
                                    <?php
                                    if ($this->session->userdata('role_id') == 2) {
                                        echo "<button type='button' class='btn btn-warning' data-toggle='modal' data-target='#import-harian'><span class='fa fa-file-text-o'></span> &nbsp;Import File Excel</button>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-xs-12">
                                    <table id="dataHarian" class="table table-bordered table-striped" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kode Stasiun</th>
                                                <th>Tanggal</th>
                                                <th>Jam</th>
                                                <th>Jml CH (mm)</th>
                                                <th>Blok Sawah</th>
                                                <th>Pengamat</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="musiman" class="tab-pane fade <?php if ($tab == "musiman") {
                                                                    echo "in active";
                                                                } ?>">
                            <br />
                            <?= $this->session->flashdata('message'); ?>
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#data-musiman">Buat Data Musiman</button>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="dataMusiman" class="table table-bordered table-striped" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kode Stasiun</th>
                                                <th>Tanggal</th>
                                                <th>Sumber Air</th>
                                                <th>Pengelolaan Air</th>
                                                <th>Jenis Tanaman</th>
                                                <th>Varietas Tanaman</th>
                                                <th>Pengamat</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div id="akhirmusim" class="tab-pane fade <?php if ($tab == "akhirmusim") {
                                                                        echo "in active";
                                                                    } ?>">
                            <br />
                            <?= $this->session->flashdata('message'); ?>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#data-akhir-musim">Buat Data Akhir Musim</button>
                            <br /><br />
                            <table id="dataAkhirMusim" class="table table-bordered table-striped" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kode Stasiun</th>
                                        <th>Tanggal</th>
                                        <th>Hasil Panen Kotor</th>
                                        <th>Cara Panen</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- Modal -->
    <div id="import-harian" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Import Data <?php echo $menu_title; ?></h4>
                </div>
                <form action="<?= base_url(); ?>admin/import_data_harian/" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Kode Stasiun :</label>
                            <select name="import_id_stasiun_harian" id="import_id_stasiun_harian" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                <option value="">Pilih Kode Stasiun</option>
                                <?php foreach ($st_pengamatan as $speng) : ?>
                                    <option value="<?= $speng['id']; ?>"><?= $speng['kd_stasiun']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label style="margin-top: 6px;">Import Data :</label>
                            <input type="file" name="import_harian_file" style="margin-top: 6px;" />
                            <br />
                            <font color="red">*</font> File yang dapat diupload xls, xlsx, csv
                            <br /><br /><br />
                            <a href="<?= base_url('assets/uploads/template/Template-Input-Data-Harian.xlsx'); ?>" download><span class="fa fa-file-text-o"></span> Download Template Import Data Harian Excel</a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Upload file</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="data-harian" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buat Data <?php echo $menu_title; ?></h4>
                </div>
                <form action="<?= base_url('admin/add_harian'); ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Kode Stasiun :</label>
                            <select name="id_stasiun_harian" id="id_stasiun_harian" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                <option value="">Pilih Kode Stasiun</option>
                                <?php foreach ($st_pengamatan as $speng) : ?>
                                    <option value="<?= $speng['id']; ?>"><?= $speng['kd_stasiun']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Tanggal :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="tanggal" name="tanggal" required />
                                    <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Jam :</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input id="jam" name="jam" type="text" value="7:30" class="form-control">
                                    <span class="input-group-addon add-on fontello-clock"></span>
                                </div>
                            </div>
                        </div>
                        <div style="font-size: 15px; margin-bottom:-10px;">
                            <b>Curah Hujan</b>
                        </div>
                        <hr />
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Sifat Hujan :</label>
                                <input type="text" class="form-control" id="sifat_hujan" name="sifat_hujan" required>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Dampak hujan pada lahan :</label>
                                <input type="text" class="form-control" id="dampak_hujan" name="dampak_hujan">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Jumlah Curah Hujan :</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="jumlah_ch" name="jumlah_ch" min="0" step="0.1">
                                    <span class="input-group-addon">mm</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Tinggi air pada lahan :</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="tinggi_air" name="tinggi_air">
                                    <span class="input-group-addon">cm</span>
                                </div>
                            </div>
                        </div>
                        <div style="font-size: 15px; margin-bottom:-10px;">
                            <b>Umur Tanaman</b>
                        </div>
                        <hr />
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">HSS :</label>
                                <input type="text" class="form-control" id="hss" name="hss">
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">HST :</label>
                                <input type="text" class="form-control" id="hst" name="hst">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit_modal_harian" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ubah Data <?php echo $menu_title; ?></h4>
                </div>
                <form action="<?= base_url('admin/edit_harian/'); ?>" method="post" id="form_harian">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Kode Stasiun :</label>
                            <input type="text" id="h_id_harian_edit" name="h_id_harian_edit" hidden>
                            <select name="h_id_stasiun_edit" id="h_id_stasiun_edit" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                <option value="">Pilih Kode Stasiun</option>
                                <?php foreach ($st_pengamatan as $speng) : ?>
                                    <option value="<?= $speng['id']; ?>"><?= $speng['kd_stasiun']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Tanggal :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="h_tanggal_edit" name="h_tanggal_edit" value="" required />
                                    <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Jam :</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input id="h_jam_edit" name="h_jam_edit" type="text" class="form-control" value="" required>
                                    <span class="input-group-addon add-on fontello-clock"></span>
                                </div>
                            </div>
                        </div>
                        <div style="font-size: 15px; margin-bottom:-10px;">
                            <b>Curah Hujan</b>
                        </div>
                        <hr />
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Sifat Hujan :</label>
                                <input type="text" class="form-control" id="h_sifat_hujan_edit" name="h_sifat_hujan_edit" value="">
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Dampak hujan pada lahan :</label>
                                <input type="text" class="form-control" id="h_dampak_hujan_edit" name="h_dampak_hujan_edit" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Jumlah Curah Hujan :</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="h_jumlah_ch_edit" name="h_jumlah_ch_edit" min="0" step="0.1" value="" required>
                                    <span class="input-group-addon">mm</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Tinggi Air pada Lahan :</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="h_tinggi_air_edit" name="h_tinggi_air_edit" value="">
                                    <span class="input-group-addon">cm</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">HSS :</label>
                                <input type="number" class="form-control" id="h_hss_edit" name="h_hss_edit" max="200" value="">
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">HST :</label>
                                <input type="number" class="form-control" id="h_hst_edit" name="h_hst_edit" value="">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="view_modal_harian" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View <?php echo $menu_title; ?></h4>
                </div>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 50%"><b>ID</b></td>
                            <td style="width: 50%"><span id="id_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Kode Stasiun</b></td>
                            <td style="width: 50%"><span id="kd_stasiun_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Tanggal</b></td>
                            <td style="width: 50%"><span id="tanggal_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Jam</b></td>
                            <td style="width: 50%"><span id="jam_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Jumlah Curah Hujan</b></td>
                            <td style="width: 50%"><span id="jumlah_ch_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Sifat Hujan</b></td>
                            <td style="width: 50%"><span id="sifat_hujan_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Dampak hujan pada lahan</b></td>
                            <td style="width: 50%"><span id="dampak_hujan_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Tinggi Air pada Lahan</b></td>
                            <td style="width: 50%"><span id="tinggi_air_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Hari Setelah Semai (HSS)</b></td>
                            <td style="width: 50%"><span id="hss_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Hari Setelah Tanam (HST)</b></td>
                            <td style="width: 50%"><span id="hst_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Blok Sawah</b></td>
                            <td style="width: 50%"><span id="blok_sawah_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Pengamat</b></td>
                            <td style="width: 50%"><span id="pengamat_view"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div id="data-musiman" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buat Data Musiman</h4>
                </div>
                <div class="controls">
                    <form action="<?= base_url('admin/add_musiman'); ?>" method="post" role="form" autocomplete="off">
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Kode Stasiun :</label>
                                    <select name="id_stasiun_musiman" id="id_stasiun_musiman" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                        <option value="">Pilih Kode Stasiun</option>
                                        <?php foreach ($st_pengamatan as $speng) : ?>
                                            <option value="<?= $speng['id']; ?>"><?= $speng['kd_stasiun']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Tanggal :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="m_tanggal" name="m_tanggal" required />
                                        <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                    </div>
                                </div>
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Pengairan</b>
                            </div>
                            <hr />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Sumber Air :</label>
                                    <input type="text" class="form-control" id="sumber_air" name="sumber_air" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Pengelolaan Air :</label>
                                    <input type="text" class="form-control" id="pengelolaan_air" name="pengelolaan_air">
                                </div>
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Tanaman</b>
                            </div>
                            <hr />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Tanaman :</label>
                                    <input type="text" class="form-control" id="jenis_tanaman" name="jenis_tanaman">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Varietas Tanaman :</label>
                                    <input type="text" class="form-control" id="varietas_tanaman" name="varietas_tanaman">
                                </div>
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Aplikasi Pestisida</b>
                            </div>
                            <hr />
                            <div class="form-group row" id="dynamic_field_pestisida">
                                <div class="col-sm-6 mb-1 mb-sm-0">
                                    <label for="usr">Tanggal :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tgl_pestisida" name="tgl_pestisida[]" required />
                                        <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Aplikasi Pestisida ke :</label>
                                    <div class="input-group">
                                        <input class="form-control" name="pestisida_ke[]" id="pestisida_ke[]" type="text" required />
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" id="btn-add-pestisida" type="button">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    <small>Tekan <span class="glyphicon glyphicon-plus gs"></span> untuk menambahkan bidang formulir lain.</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Pestisida :</label>
                                    <input type="text" class="form-control" id="jenis_pestisida" name="jenis_pestisida">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dosis Pestisida :</label>
                                    <input type="text" class="form-control" id="dosis_pestisida" name="dosis_pestisida">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="usr">Tujuan Aplikasi Pestisida :</label>
                                <input type="text" class="form-control" id="tujuan_pestisida" name="tujuan_pestisida">
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Pemupukan</b>
                            </div>
                            <hr />
                            <div class="form-group row" id="dynamic_field_pemupukan">
                                <div class="col-sm-6 mb-1 mb-sm-0">
                                    <label for="usr">Tanggal :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="tgl_pemupukan" name="tgl_pemupukan[]" required />
                                        <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Tahap Pemupukan ke :</label>
                                    <div class="input-group">
                                        <input class="form-control" name="pemupukan_ke[]" id="pemupukan_ke[]" type="text" required />
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" id="btn-add-pemupukan" type="button">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    <small>Tekan <span class="glyphicon glyphicon-plus gs"></span> untuk menambahkan bidang formulir lain.</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Pupuk :</label>
                                    <input type="text" class="form-control" id="jenis_pupuk" name="jenis_pupuk">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dosis Pupuk :</label>
                                    <input type="text" class="form-control" id="dosis_pupuk" name="dosis_pupuk">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="usr">Tujuan Pemupukan :</label>
                                <input type="text" class="form-control" id="tujuan_pemupukan" name="tujuan_pemupukan">
                            </div>
                            <br />
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Organisme Penganggu Tanaman (OPT)</b>
                            </div>
                            <hr />
                            <div style="font-size: 14px; margin-bottom:-10px; text-align:right;">
                                <b> - Hama - </b>
                            </div>
                            <br />
                            <br />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Hama :</label>
                                    <input type="text" class="form-control" id="jenis_hm" name="jenis_hm">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Intensitas Serangan :</label>
                                    <input type="text" class="form-control" id="int_serangan_hm" name="int_serangan_hm">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Lama Serangan :</label>
                                    <input type="text" class="form-control" id="lama_serangan_hm" name="lama_serangan_hm">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dampak Pada Tanaman :</label>
                                    <input type="text" class="form-control" id="dampak_hm" name="dampak_hm">
                                </div>
                            </div>
                            <div style="font-size: 14px; margin-bottom:-10px; text-align:right;">
                                <b> - Penyakit - </b>
                            </div>
                            <br />
                            <br />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Penyakit :</label>
                                    <input type="text" class="form-control" id="jenis_pyt" name="jenis_pyt">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Intensitas Serangan :</label>
                                    <input type="text" class="form-control" id="int_serangan_pyt" name="int_serangan_pyt">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Lama Serangan :</label>
                                    <input type="text" class="form-control" id="lama_serangan_pyt" name="lama_serangan_pyt">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dampak Pada Tanaman :</label>
                                    <input type="text" class="form-control" id="dampak_pyt" name="dampak_pyt">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="edit_modal_musiman" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buat Data Musiman</h4>
                </div>
                <div class="controls">
                    <form action="<?= base_url('admin/edit_musiman'); ?>" method="post" id="form_musiman">
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Kode Stasiun :</label>
                                    <input type="text" id="m_id_musiman_edit" name="m_id_musiman_edit" hidden>
                                    <select name="m_id_stasiun_edit" id="m_id_stasiun_edit" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                        <option value="">Pilih Kode Stasiun</option>
                                        <?php foreach ($st_pengamatan as $speng) : ?>
                                            <option value="<?= $speng['id']; ?>"><?= $speng['kd_stasiun']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Tanggal :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="m_tanggal_edit" name="m_tanggal_edit" required />
                                        <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                    </div>
                                </div>
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Pengairan</b>
                            </div>
                            <hr />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Sumber Air :</label>
                                    <input type="text" class="form-control" id="m_sumber_air_edit" name="m_sumber_air_edit" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Pengelolaan Air :</label>
                                    <input type="text" class="form-control" id="m_pengelolaan_air_edit" name="m_pengelolaan_air_edit">
                                </div>
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Tanaman</b>
                            </div>
                            <hr />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Tanaman :</label>
                                    <input type="text" class="form-control" id="m_jenis_tanaman_edit" name="m_jenis_tanaman_edit">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Varietas Tanaman :</label>
                                    <input type="text" class="form-control" id="m_varietas_tanaman_edit" name="m_varietas_tanaman_edit">
                                </div>
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Aplikasi Pestisida</b>
                            </div>
                            <hr />
                            <div class="form-group row" id="dynamic_pes_edit">
                                <div class="col-sm-6 mb-1 mb-sm-0">
                                    <label for="usr">Tanggal :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="m_edit_tgl_pes" name="m_edit_tgl_pes[]" required />
                                        <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Aplikasi Pestisida ke :</label>
                                    <div class="input-group">
                                        <input class="form-control" name="m_edit_pes_ke[]" id="m_edit_pes_ke[]" type="text" required />
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" id="btn-edit-pestisida" type="button">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    <small>Tekan <span class="glyphicon glyphicon-plus gs"></span> untuk menambahkan bidang formulir lain.</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Pestisida :</label>
                                    <input type="text" class="form-control" id="m_jenis_pestisida_edit" name="m_jenis_pestisida_edit">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dosis Pestisida :</label>
                                    <input type="text" class="form-control" id="m_dosis_pestisida_edit" name="m_dosis_pestisida_edit">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="usr">Tujuan Aplikasi Pestisida :</label>
                                <input type="text" class="form-control" id="m_tujuan_pestisida_edit" name="m_tujuan_pestisida_edit">
                            </div>
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Pemupukan</b>
                            </div>
                            <hr />
                            <div class="form-group row" id="dynamic_pem_edit">
                                <div class="col-sm-6 mb-1 mb-sm-0">
                                    <label for="usr">Tanggal :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="m_edit_tgl_pem" name="m_edit_tgl_pem[]" required />
                                        <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Tahap Pemupukan ke :</label>
                                    <div class="input-group">
                                        <input class="form-control" name="m_edit_pem_ke[]" id="m_edit_pem_ke[]" type="text" required />
                                        <span class="input-group-btn">
                                            <button class="btn btn-success" id="btn-edit-pemupukan" type="button">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    <small>Tekan <span class="glyphicon glyphicon-plus gs"></span> untuk menambahkan bidang formulir lain.</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Pupuk :</label>
                                    <input type="text" class="form-control" id="m_jenis_pupuk_edit" name="m_jenis_pupuk_edit">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dosis Pupuk :</label>
                                    <input type="text" class="form-control" id="m_dosis_pupuk_edit" name="m_dosis_pupuk_edit">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="usr">Tujuan Pemupukan :</label>
                                <input type="text" class="form-control" id="m_tujuan_pemupukan_edit" name="m_tujuan_pemupukan_edit">
                            </div>
                            <br />
                            <div style="font-size: 15px; margin-bottom:-10px;">
                                <b>Organisme Penganggu Tanaman (OPT)</b>
                            </div>
                            <hr />
                            <div style="font-size: 14px; margin-bottom:-10px; text-align:right;">
                                <b> - Hama - </b>
                            </div>
                            <br />
                            <br />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Hama :</label>
                                    <input type="text" class="form-control" id="m_jenis_hm_edit" name="m_jenis_hm_edit">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Intensitas Serangan :</label>
                                    <input type="text" class="form-control" id="m_int_serangan_hm_edit" name="m_int_serangan_hm_edit">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Lama Serangan :</label>
                                    <input type="text" class="form-control" id="m_lama_serangan_hm_edit" name="m_lama_serangan_hm_edit">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dampak Pada Tanaman :</label>
                                    <input type="text" class="form-control" id="m_dampak_hm_edit" name="m_dampak_hm_edit">
                                </div>
                            </div>
                            <div style="font-size: 14px; margin-bottom:-10px; text-align:right;">
                                <b> - Penyakit - </b>
                            </div>
                            <br />
                            <br />
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Jenis Penyakit :</label>
                                    <input type="text" class="form-control" id="m_jenis_pyt_edit" name="m_jenis_pyt_edit">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Intensitas Serangan :</label>
                                    <input type="text" class="form-control" id="m_int_serangan_pyt_edit" name="m_int_serangan_pyt_edit">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Lama Serangan :</label>
                                    <input type="text" class="form-control" id="m_lama_serangan_pyt_edit" name="m_lama_serangan_pyt_edit">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Dampak Pada Tanaman :</label>
                                    <input type="text" class="form-control" id="m_dampak_pyt_edit" name="m_dampak_pyt_edit">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="view_modal_musiman" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View Musiman</h4>
                </div>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td style="width: 50%"><b>ID</b></td>
                            <td style="width: 50%"><span id="m_id_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Kode Stasiun</b></td>
                            <td style="width: 50%"><span id="m_kd_stasiun_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Tanggal</b></td>
                            <td style="width: 50%"><span id="m_tanggal_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Pengamat</b></td>
                            <td style="width: 50%"><span id="m_first_name_view"></span> <span id="m_last_name_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Sumber Air</b></td>
                            <td style="width: 50%"><span id="m_sumber_air_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Pengelolaan Air</b></td>
                            <td style="width: 50%"><span id="m_pengelolaan_air_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Jenis Tanaman</b></td>
                            <td style="width: 50%"><span id="m_jenis_tanaman_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Varietas Tanaman</b></td>
                            <td style="width: 50%"><span id="m_varietas_tanaman_view"></span></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <br />
                    <div style="font-size: 15px; margin-bottom:-10px; margin-left:5px;">
                        <b>Aplikasi Pestisida</b>
                    </div>
                    <br />
                    <tbody id="m_pestisida_view">
                        <!-- pestisida -->
                        <tr>
                            <td style="width: 50%"><b>Jenis Pestisida</b></td>
                            <td style="width: 50%"><span id="m_janis_pestisida_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Dosis Pestisida</b></td>
                            <td style="width: 50%"><span id="m_dosis_pestisida_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Tujuan Pestisida</b></td>
                            <td style="width: 50%"><span id="m_tujuan_pestisida_view"></span></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <br />
                    <div style="font-size: 15px; margin-bottom:-10px; margin-left:5px;">
                        <b>Pemupukan</b>
                    </div>
                    <br />
                    <tbody id="m_pemupukan_view">
                        <!-- pupuk -->
                        <tr>
                            <td style="width: 50%"><b>Jenis Pupuk</b></td>
                            <td style="width: 50%"><span id="m_jenis_pupuk_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Dosis Pupuk</b></td>
                            <td style="width: 50%"><span id="m_dosis_pupuk_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Tujuan Pemupukan</b></td>
                            <td style="width: 50%"><span id="m_tujuan_pupuk_view"></span></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <br />
                    <div style="font-size: 15px; margin-bottom:-10px; margin-left:5px;">
                        <b>Organisme Penganggu Tanaman (OPT)</b>
                    </div>
                    <hr />
                    <div style="font-size: 14px; margin-bottom:-10px; text-align:right; margin-right:15px;">
                        <b> - Hama - </b>
                    </div>
                    <br />
                    <tbody>
                        <tr>
                            <td style="width: 50%"><b>Jenis Hama</b></td>
                            <td style="width: 50%"><span id="m_jenis_hm_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Intensitas Serangan Hama</b></td>
                            <td style="width: 50%"><span id="m_int_serangan_hm_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Lama Serangan</b></td>
                            <td style="width: 50%"><span id="m_lama_serangan_hm_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Dampak Pada Tanaman</b></td>
                            <td style="width: 50%"><span id="m_dampak_tanaman_hm_view"></span></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <br />
                    <div style="font-size: 14px; margin-bottom:-10px; text-align:right; margin-right:15px;">
                        <b> - Penyakit - </b>
                    </div>
                    <br />
                    <tbody>
                        <tr>
                            <td style="width: 50%"><b>Jenis Penyakit</b></td>
                            <td style="width: 50%"><span id="m_jenis_pyt_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Intensitas Serangan</b></td>
                            <td style="width: 50%"><span id="m_int_serangan_pyt_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Lama Serangan</b></td>
                            <td style="width: 50%"><span id="m_lama_serangan_pyt_view"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 50%"><b>Dampak Pada Tanaman</b></td>
                            <td style="width: 50%"><span id="m_dampak_tanaman_pyt_view"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <div id="data-akhir-musim" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buat Data Akhir Musim</h4>
                </div>
                <form action="<?= base_url('admin/add_akhir_musim'); ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Kode Stasiun :</label>
                                <select name="id_stasiun_amusim" id="id_stasiun_amusim" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Kode Stasiun</option>
                                    <?php foreach ($st_pengamatan as $speng) : ?>
                                        <option value="<?= $speng['id']; ?>"><?= $speng['kd_stasiun']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Tanggal :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="a_tanggal" name="a_tanggal" required />
                                    <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Hasil Panen (Kotor) :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="hasi_panen" name="hasil_panen" required />
                                    <span class="input-group-addon">Kg</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Cara Panen :</label>
                                <select name="cara_panen" id="cara_panen" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Cara Panen</option>
                                    <option value="Gebot">Gebot</option>
                                    <option value="Grabag">Grabag</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit_modal_amusim" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buat Data Akhir Musim</h4>
                </div>
                <form action="<?= base_url('admin/edit_akhir_musim/'); ?>" method="post" id="form_akhir_musim">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Kode Stasiun :</label>
                                <input type="text" id="a_id_amusim_edit" name="a_id_amusim_edit" hidden>
                                <select name="a_id_stasiun_amusim_edit" id="a_id_stasiun_amusim_edit" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Kode Stasiun</option>
                                    <?php foreach ($st_pengamatan as $speng) : ?>
                                        <option value="<?= $speng['id']; ?>"><?= $speng['kd_stasiun']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Tanggal :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="a_tanggal_edit" name="a_tanggal_edit" required />
                                    <span style="cursor:pointer;" class="input-group-addon fontello-calendar-1"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Hasil Panen (Kotor) :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="a_hasi_panen_edit" name="a_hasi_panen_edit" value="" required />
                                    <span class="input-group-addon">Kg</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Cara Panen :</label>
                                <select name="a_cara_panen_edit" id="a_cara_panen_edit" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Cara Panen</option>
                                    <option value="Gebot">Gebot</option>
                                    <option value="Grabag">Grabag</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="view_modal_amusim" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">View Akhir Musim</h4>
                </div>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td><b>ID</b></td>
                            <td><span id="a_id_amusim_view"></span></td>
                        </tr>
                        <tr>
                            <td><b>Kode Stasiun</b></td>
                            <td><span id="a_kd_stasiun_view"></span></td>
                        </tr>
                        <tr>
                            <td><b>Tanggal</b></td>
                            <td><span id="a_tanggal_view"></span></td>
                        </tr>
                        <tr>
                            <td><b>Pengamat</b></td>
                            <td><span id="a_pengamat_view"></span></td>
                        </tr>
                        <tr>
                            <td><b>Hasil Panen (Kotor)</b></td>
                            <td><span id="a_hasil_panen_view"></span></td>
                        </tr>
                        <tr>
                            <td><b>Cara Panen</b></td>
                            <td><span id="a_cara_panen_view"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->