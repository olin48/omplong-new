<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">

                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data <?php echo $menu_title; ?></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <?= $this->session->flashdata('message'); ?>
                    <?php if ($role_id != 5) {
                        echo "<button type='button' class='btn btn-success' data-toggle='modal' data-target='#myModal'>Buat " . $menu_title . "</button>";
                    } ?>
                    <br /><br />
                    <table id="dataSawah" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Stasiun</th>
                                <th>Nama Desa</th>
                                <th>Blok Sawah</th>
                                <th>Kelompok Tani</th>
                                <th>Pengamat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($stasiun as $s) { ?>
                                <tr>
                                    <td width="5%"><?= $i++; ?></td>
                                    <td><?= $s['kd_stasiun']; ?></td>
                                    <td><?= $s['nm_desa']; ?></td>
                                    <td><?= $s['blok_sawah']; ?></td>
                                    <td><?php if ($s['kelompok_tani'] == "") {
                                            echo "<span style='font-style:italic; color:red;'>(belum diset)</span>";
                                        } else {
                                            echo $s['kelompok_tani'];
                                        } ?>
                                    </td>
                                    <td><?= $s['first_name']; ?> <?= $s['last_name']; ?></td>
                                    <td align="center" width="12%">
                                        <?php if ($role_id != 5) {
                                            echo "<button class='btn-link d-view' data-toggle='modal' data-target='#modal_view_stasiun_" . $s['id'] . "' title='Lihat'>" .
                                                "<i class='fontello-eye-outline' style='color:blue; font-size:15px;'></i>" .
                                                "</button>" .
                                                "<button class='btn-link d-edit' data-toggle='modal' data-target='#modal_edit_stasiun_" . $s['id'] . "' title='Ubah'>" .
                                                "<i class='fa fa-edit' style='color:green; font-size:13px;'></i>" .
                                                "</button>" .
                                                "<a title='Delete' href='" . base_url('admin/delete_stasiun/') . $s['id'] . "' onclick='return confirm(" . '"Anda yakin ingin menghapus data ini?"' . ")' type='button' class='btn-link d-delete'>" .
                                                "<i class='fontello-trash-2' style='color:red; font-size:12px; padding-left:10px;'></i>" .
                                                "</a>";
                                        } else {
                                            echo "<button class='btn-link d-view' data-toggle='modal' data-target='#modal_view_stasiun_" . $s['id'] . "' title='Lihat'>" .
                                                "<i class='fontello-eye-outline' style='color:blue; font-size:15px;'></i>" .
                                                "</button>";
                                        } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buat <?php echo $menu_title; ?></h4>
                </div>
                <form action="<?= base_url('admin/stasiun_pengamatan'); ?>" method="post">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Pengamat :</label>
                                <select name="id_pemilik" id="id_pemilik" class="selectpicker" data-live-search="true" data-width="100%" required>
                                    <option value="">Pilih Pengamat</option>
                                    <?php foreach ($pengamat as $p) : ?>
                                        <option value="<?= $p['id']; ?>"><?= $p['first_name']; ?> <?= $p['last_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Kode Stasiun :</label>
                                <input type="text" class="form-control" id="kd_stasiun" name="kd_stasiun" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Luas Lahan :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="luas_tanah" name="luas_tanah">
                                    <span class="input-group-addon">m</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Jenis Tanah :</label>
                                <input type="text" class="form-control" id="jenis_tanah" name="jenis_tanah">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="usr">Latitude :</label>
                                <input type="text" class="form-control" id="latitude" name="latitude">
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Longitude :</label>
                                <input type="text" class="form-control" id="longitude" name="longitude">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="usr">Kab/Kota :</label>
                            <select name="id_kabkot" id="id_kabkot" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required" <?php if ($id_kabkota != null) {
                                                                                                                                                                    echo "disabled";
                                                                                                                                                                } ?>>
                                <option value="">Pilih Kab/Kota</option>
                                <?php foreach ($kabkota as $k) : ?>
                                    <option <?php if ($id_kabkota == $k['id']) {
                                                echo "selected=selected";
                                            } ?> value="<?= $k['id']; ?>"><?= $k['nm_kabkota']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Wilayah :</label>
                            <select name="id_wilayah" id="id_wilayah" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required" <?php if ($id_kabkota != null || $id_korwil != null) {
                                                                                                                                                                    echo "disabled";
                                                                                                                                                                } ?>>
                                <option value="">Pilih Wilayah</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Kecamatan/Desa :</label>
                            <select name="id_desa" id="id_desa" class="selectpicker" data-live-search="true" data-width="100%" required>
                                <option value="">Pilih ...</option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="usr">Blok Sawah :</label>
                                <input type="text" class="form-control" id="blok_sawah" name="blok_sawah">
                            </div>
                            <div class="col-sm-6">
                                <label for="usr">Kelompok Tani :</label>
                                <input type="text" class="form-control" id="kelompok_tani" name="kelompok_tani">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php foreach ($stasiun as $s) { ?>
        <div id="modal_edit_stasiun_<?= $s['id'] ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit <?php echo $menu_title; ?></h4>
                    </div>
                    <form action="<?= base_url('admin/edit_stasiun/'); ?><?= $s['id']; ?>" method="post">
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Pengamat :</label>
                                    <select name="id_pemilik_<?= $s['id']; ?>" id="id_pemilik_<?= $s['id']; ?>" class="selectpicker" data-live-search="true" data-width="100%" required>
                                        <option value="">Pilih Pengamat</option>
                                        <?php foreach ($pengamat as $p) : ?>
                                            <option <?php if ($s['id_user'] == $p['id']) {
                                                        echo "selected=selected";
                                                    } ?> value="<?= $p['id']; ?>"><?= $p['first_name']; ?> <?= $p['last_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Kode Stasiun :</label>
                                    <input type="text" class="form-control" id="kd_stasiun" name="kd_stasiun" value="<?= $s['kd_stasiun']; ?>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Luas Lahan :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="luas_tanah" name="luas_tanah" value="<?= $s['luas']; ?>">
                                        <span class="input-group-addon">m</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Jenis Tanah :</label>
                                    <input type="text" class="form-control" id="jenis_tanah" name="jenis_tanah" value="<?= $s['jenis_tanah']; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="usr">Latitude :</label>
                                    <input type="text" class="form-control" id="latitude" name="latitude" value="<?= $s['latitude']; ?>">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Longitude :</label>
                                    <input type="text" class="form-control" id="longitude" name="longitude" value="<?= $s['latitude']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="usr">Kab/Kota :</label>
                                <select name="id_kabkot_<?= $s['id']; ?>" id="id_kabkot_<?= $s['id']; ?>" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Kab/Kota</option>
                                    <?php foreach ($kabkota as $k) : ?>
                                        <option <?php if ($s['id_kabkota'] == $k['id']) {
                                                    echo "selected=selecetd";
                                                } ?> value="<?= $k['id']; ?>"><?= $k['nm_kabkota']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="usr">Wilayah :</label>
                                <select name="id_wilayah_<?= $s['id']; ?>" id="id_wilayah_<?= $s['id']; ?>" class="selectpicker" data-live-search="true" data-width="100%" data-validation="required">
                                    <option value="">Pilih Wilayah</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="usr">Kecamatan/Desa :</label>
                                <select name="id_desa_<?= $s['id']; ?>" id="id_desa_<?= $s['id']; ?>" class="selectpicker" data-live-search="true" data-width="100%" required>
                                    <option value="">Pilih ...</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label for="usr">Blok Sawah :</label>
                                    <input type="text" class="form-control" id="blok_sawah" name="blok_sawah" value="<?= $s['blok_sawah']; ?>">
                                </div>
                                <div class="col-sm-6">
                                    <label for="usr">Kelompok Tani :</label>
                                    <input type="text" class="form-control" id="kelompok_tani" name="kelompok_tani" value="<?= $s['kelompok_tani']; ?>">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php foreach ($stasiun as $s) { ?>
        <div id="modal_view_stasiun_<?= $s['id'] ?>" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View <?php echo $menu_title; ?></h4>
                    </div>
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><b>Kode Stasiun</b></td>
                                <td><?= $s['kd_stasiun']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Blok Sawah</b></td>
                                <td><?= $s['blok_sawah']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Kabupaten/Kota</b></td>
                                <td><?= $s['nm_kabkota']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Kecamatan/Desa</b></td>
                                <td><?= $s['nm_kecamatan']; ?> - <?= $s['nm_desa']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Pemilik</b></td>
                                <td><?= $s['first_name']; ?> <?= $s['last_name']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Kelompok Tani</b></td>
                                <td><?= $s['kelompok_tani']; ?></td>
                            </tr>
                            <tr>
                                <td><b>Latitude</b></td>
                                <td><?php if ($s['latitude'] == 0.00000000) {
                                        echo "<span style='font-style:italic; color:red;'>(belum diset)</span>";
                                    } else {
                                        echo $s['latitude'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Longitude</b></td>
                                <td><?php if ($s['longitude'] == 0.00000000) {
                                        echo "<span style='font-style:italic; color:red;'>(belum diset)</span>";
                                    } else {
                                        echo $s['longitude'];
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Luas Tanah</b></td>
                                <td><?php if ($s['luas'] == 0) {
                                        echo "<span style='font-style:italic; color:red;'>(belum diset)</span>";
                                    } else {
                                        echo $s['luas'];
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Jenis Tanah</b></td>
                                <td><?php if ($s['jenis_tanah'] == "") {
                                        echo "<span style='font-style:italic; color:red;'>(belum diset)</span>";
                                    } else {
                                        echo $s['jenis_tanah'];
                                    } ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

</div>
<!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->