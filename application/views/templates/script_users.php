<script>
    (function($) {
        $('#kabkota_id').prop('disabled', true);
        $('#korwil_id').prop('disabled', true);
        $('#role_id').change(function() {
            var role_id = $(this).children("option:selected").val();
            var kabkota_id = $('#kabkota_id').val();
            if (role_id == "3") {
                $('#kabkota_id').prop('disabled', false);
                $('#kabkota_id').attr('required', true);
                $('#korwil_id').prop('disabled', true);
                $('#korwil_id option[value=""]').attr('selected', 'selected');
                $.getJSON(
                    'get_data_kabkota',
                    function(result) {
                        $('#kabkota_id').empty();
                        $('#kabkota_id').attr('required', true);
                        $('#kabkota_id').append('<option value="">Pilih Kab/Kota</option>');
                        $.each(result.result, function() {
                            $('#kabkota_id').append('<option value="' + this['id'] + '">' + this['nm_kabkota'] + '</option>');
                        });
                    }
                );
            } else if (role_id == "4") {
                $('#kabkota_id').prop('disabled', false);
                $.getJSON(
                    'get_data_kabkota',
                    function(result) {
                        $('#kabkota_id').empty();
                        $('#kabkota_id').attr('required', true);
                        $('#kabkota_id').append('<option value="">Pilih Kab/Kota</option>');
                        $.each(result.result, function() {
                            $('#kabkota_id').append('<option value="' + this['id'] + '">' + this['nm_kabkota'] + '</option>');
                        });
                    }
                );
                if (kabkota_id == "") {
                    $('#korwil_id').prop('disabled', true);
                    $('#korwil_id option[value=""]').attr('selected', 'selected');
                } else {
                    $('#korwil_id').prop('disabled', false);
                    $('#korwil_id').attr('required', true);
                    $.getJSON(
                        'get_data_korwil/' + kabkota_id,
                        function(result) {
                            $('#korwil_id').empty();
                            $('#korwil_id').append('<option value="">Pilih Korwil</option>');
                            $.each(result.result, function() {
                                $('#korwil_id').append('<option value="' + this['id'] + '">' + this['cd_korwil'] + ' - ' + this['nm_korwil'] + '</option>');
                            });
                        }
                    );
                }
            } else {
                // alert('masuk');
                $('#kabkota_id').attr('required', false);
                $('#korwil_id').attr('required', false);
                $('#kabkota_id').prop('disabled', true);
                $('#korwil_id').prop('disabled', true);
                $('#kabkota_id option[value=""]').attr('selected', 'selected');
                $('#korwil_id option[value=""]').attr('selected', 'selected');
            }
        });

        $('#kabkota_id').change(function() {
            var kabkota_id = $(this).children("option:selected").val();
            var role_id = $('#role_id').val();
            if (role_id == "3") {
                $('#korwil_id').prop('disabled', true);
            } else if (role_id == "4") {
                if (kabkota_id == "") {
                    $('#korwil_id').prop('disabled', true);
                    $('#korwil_id option[value=""]').attr('selected', 'selected');
                } else {
                    $('#korwil_id').prop('disabled', false);
                    $('#korwil_id').attr('required', true);
                    $.getJSON(
                        'get_data_korwil/' + kabkota_id,
                        function(result) {
                            $('#korwil_id').empty();
                            $('#korwil_id').append('<option value="">Pilih Korwil</option>');
                            $.each(result.result, function() {
                                $('#korwil_id').append('<option value="' + this['id'] + '">' + this['cd_korwil'] + ' - ' + this['nm_korwil'] + '</option>');
                            });
                        }
                    );
                }
            }
        });
    })(jQuery);
</script>

</body>

</html>