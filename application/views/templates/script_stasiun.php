<script>
    (function($) {
        $('#id_desa').prop('disabled', true);
        $('#id_wilayah').prop('disabled', true);
        if ($('#id_kabkot').val() != null) {
            $.getJSON(
                'desa_data/' + $('#id_kabkot').val(),
                function(result) {
                    $('#id_desa').empty();
                    $('#id_desa').append('<option value="">Pilih ...</option>');
                    $.each(result.result, function() {
                        $('#id_desa').append('<option value="' + this['id'] + '">' + this['nm_kecamatan'] + ' - ' + this['nm_desa'] + '</option>');
                        $('#id_desa').prop('disabled', false);
                    });
                    $('#id_desa').selectpicker("refresh");
                }
            );

            $.getJSON(
                'wilayah_data/' + $('#id_kabkot').val(),
                function(result) {
                    $('#id_wilayah').empty();
                    $('#id_wilayah').append('<option value="">Pilih ...</option>');
                    $.each(result.result, function() {
                        $('#id_wilayah').append('<option value="' + this['id'] + '">' + this['nm_korwil'] + '</option>');
                        $('#id_wilayah').prop('disabled', false);
                    });
                    $('#id_wilayah').selectpicker("refresh");
                }
            );
        }
        $('#id_kabkot').change(function() {
            var id_kabkot = $(this).children("option:selected").val();
            if (id_kabkot == "") {
                $('#id_desa option[value=""]').attr('selected', 'selected');
                $('#id_desa').selectpicker("refresh");
                $('#id_desa').prop('disabled', true);
                $('#id_wilayah option[value=""]').attr('selected', 'selected');
                $('#id_wilayah').selectpicker("refresh");
                $('#id_wilayah').prop('disabled', true);
            } else {
                $.getJSON(
                    'desa_data/' + $('#id_kabkot').val(),
                    function(result) {
                        $('#id_desa').empty();
                        $('#id_desa').append('<option value="">Pilih ...</option>');
                        $.each(result.result, function() {
                            $('#id_desa').append('<option value="' + this['id'] + '">' + this['nm_kecamatan'] + ' - ' + this['nm_desa'] + '</option>');
                            $('#id_desa').prop('disabled', false);
                        });
                        $('#id_desa').selectpicker("refresh");
                    }
                );

                $.getJSON(
                    'wilayah_data/' + $('#id_kabkot').val(),
                    function(result) {
                        $('#id_wilayah').empty();
                        $('#id_wilayah').append('<option value="">Pilih ...</option>');
                        $.each(result.result, function() {
                            $('#id_wilayah').append('<option value="' + this['id'] + '">' + this['nm_korwil'] + '</option>');
                            $('#id_wilayah').prop('disabled', false);
                        });
                        $('#id_wilayah').selectpicker("refresh");
                    }
                );
            }
        });


        <?php foreach ($stasiun as $s) : ?>
            var id_kabkot = $('#id_kabkot_<?= $s['id']; ?>').val();
            if (id_kabkot == "") {
                $('#id_desa_<?= $s['id']; ?>').prop('disabled', true);
                $('#id_wilayah_<?= $s['id']; ?>').prop('disabled', true);
            } else {
                $('#id_desa_<?= $s['id']; ?>').prop('disabled', false);
                $('#id_wilayah_<?= $s['id']; ?>').prop('disabled', false);
                $.getJSON(
                    'desa_data/' + id_kabkot,
                    function(result) {
                        $('#id_desa_<?= $s['id']; ?>').empty();
                        $('#id_desa_<?= $s['id']; ?>').append('<option value="">Pilih ...</option>');
                        $.each(result.result, function() {
                            if (this['id'] == <?= $s['id_desa']; ?>) {
                                var desa_select = "selected=selected";
                            }
                            $('#id_desa_<?= $s['id']; ?>').append('<option ' + desa_select + ' value="' + this['id'] + '">' + this['nm_kecamatan'] + ' - ' + this['nm_desa'] + '</option>');
                            $('#id_desa_<?= $s['id']; ?>').prop('disabled', false);
                        });
                        $('#id_desa_<?= $s['id']; ?>').selectpicker("refresh");
                    }
                );

                $.getJSON(
                    'wilayah_data/' + id_kabkot,
                    function(result) {
                        $('#id_wilayah_<?= $s['id']; ?>').empty();
                        $('#id_wilayah_<?= $s['id']; ?>').append('<option value="">Pilih ...</option>');
                        $.each(result.result, function() {
                            if (this['id'] == <?= $s['id_wilayah']; ?>) {
                                var wilayah_select = "selected=selected";
                            }
                            $('#id_wilayah_<?= $s['id']; ?>').append('<option ' + wilayah_select + ' value="' + this['id'] + '">' + this['nm_korwil'] + '</option>');
                            $('#id_wilayah_<?= $s['id']; ?>').prop('disabled', false);
                        });
                        $('#id_wilayah_<?= $s['id']; ?>').selectpicker("refresh");
                    }
                );
            }

            $('#id_kabkot_<?= $s['id']; ?>').change(function() {
                var id_kabkot = $(this).children("option:selected").val();
                if (id_kabkot == "") {
                    $('#id_desa_<?= $s['id']; ?> option[value=""]').attr('selected', 'selected');
                    $('#id_desa_<?= $s['id']; ?>').selectpicker("refresh");
                    $('#id_desa_<?= $s['id']; ?>').prop('disabled', true);
                    $('#id_wilayah_<?= $s['id']; ?> option[value=""]').attr('selected', 'selected');
                    $('#id_wilayah_<?= $s['id']; ?>').selectpicker("refresh");
                    $('#id_wilayah_<?= $s['id']; ?>').prop('disabled', true);
                } else {
                    $.getJSON(
                        'desa_data/' + id_kabkot,
                        function(result) {
                            $('#id_desa_<?= $s['id']; ?>').empty();
                            $('#id_desa_<?= $s['id']; ?>').append('<option value="">Pilih ...</option>');
                            $.each(result.result, function() {
                                $('#id_desa_<?= $s['id']; ?>').append('<option value="' + this['id'] + '">' + this['nm_kecamatan'] + ' - ' + this['nm_desa'] + '</option>');
                                $('#id_desa_<?= $s['id']; ?>').prop('disabled', false);
                            });
                            $('#id_desa_<?= $s['id']; ?>').selectpicker("refresh");
                        }
                    );

                    $.getJSON(
                        'wilayah_data/' + id_kabkot,
                        function(result) {
                            $('#id_wilayah_<?= $s['id']; ?>').empty();
                            $('#id_wilayah_<?= $s['id']; ?>').append('<option value="">Pilih ...</option>');
                            $.each(result.result, function() {
                                $('#id_wilayah_<?= $s['id']; ?>').append('<option value="' + this['id'] + '">' + this['nm_korwil'] + '</option>');
                                $('#id_wilayah_<?= $s['id']; ?>').prop('disabled', false);
                            });
                            $('#id_wilayah_<?= $s['id']; ?>').selectpicker("refresh");
                        }
                    );
                }
            });
        <?php endforeach ?>
    })(jQuery);
</script>

</Body>

</html>