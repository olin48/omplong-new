<script>
    (function($) {
        $('#tahun').prop('disabled', true);
        $('#id_sawah').change(function() {
            var id_sawah = $(this).children("option:selected").val();
            if (id_sawah == "") {
                $('#tahun option[value=""]').attr('selected', 'selected');
                $('#tahun').prop('disabled', true);
            } else {
                $.getJSON(
                    'tahun_data/' + $('#id_sawah').val(),
                    function(result) {
                        $('#tahun').empty();
                        $('#tahun').append('<option value="">Pilih...</option>');
                        $.each(result.result, function() {
                            $('#tahun').append('<option value="' + this['tahun'] + '">' + this['tahun'] + '</option>');
                            $('#tahun').prop('disabled', false);
                        });
                    }
                )
            }
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            var target = $(e.target).attr("href") // activated tab
            if (target == "#grafik_bulanan_tahunan") {
                if ($('#id_sawah').val() != "" && $('#tahun').val() != "") {
                    $('#dataGrafik').prop('hidden', false);
                    $('#dataBoxPlot').prop('hidden', true);
                    $('#tampilkan_data_grafik').click();
                } else {
                    $('#dataGrafik').prop('hidden', true);
                    $('#dataBoxPlot').prop('hidden', true);
                }
            } else if (target == "#grafik_box_plot") {
                if ($('#id_sawah_box').val() != "") {
                    $('#dataGrafik').prop('hidden', true);
                    $('#dataBoxPlot').prop('hidden', false);
                    $('#tampilkan_data_box_plot').click();
                } else {
                    $('#dataGrafik').prop('hidden', true);
                    $('#dataBoxPlot').prop('hidden', true);
                }
            }
        });

        $('#tampilkan_data_grafik').click(function() {
            if ($('#id_sawah').val() == "") {
                $('#dataGrafik').prop('hidden', true);
                alert("Stasiun Pengamatan wajib diisi.");
            } else if ($('#tahun').val() == "") {
                $('#dataGrafik').prop('hidden', true);
                alert("Tahun wajib diisi.");
            } else {
                $('#dataGrafik').prop('hidden', false);
                $('#dataBoxPlot').prop('hidden', true);
            }

            var sawah = $('#id_sawah').val();
            var tahun = $('#tahun').val();
            var dasarian1 = [];
            var dasarian2 = [];
            var dasarian3 = [];
            $.getJSON(
                'dasarian_data_satu/' + tahun + '/' + sawah,
                function(result) {
                    $('#headBulan').empty();
                    $('#headBulan').append('<th></th>');
                    $('#bodyDasarianSatu').empty();
                    $('#bodyDasarianSatu').append('<td style="width:20%;">Dasarian 1</td>');
                    $.each(result.result, function() {
                        $('#headBulan').append('<th>' + this['bulan'] + '</th>');
                        $('#bodyDasarianSatu').append('<td>' + this['dasarian_1'] + '</td>');
                        $('#headBulan').prop('disabled', false);
                        dasarian1.push(this['dasarian_1']);
                    });
                }
            );

            $.getJSON(
                'dasarian_data_dua/' + tahun + '/' + sawah,
                function(result) {
                    $('#bodyDasarianDua').empty();
                    $('#bodyDasarianDua').append('<td>Dasarian 2</td>');
                    $.each(result.result, function() {
                        $('#bodyDasarianDua').append('<td>' + this['dasarian_2'] + '</td>');
                        dasarian2.push(this['dasarian_2']);
                    });
                }
            );

            $.getJSON(
                'dasarian_data_tiga/' + tahun + '/' + sawah,
                function(result) {
                    $('#bodyDasarianTiga').empty();
                    $('#bodyDasarianTiga').append('<td>Dasarian 3</td>');
                    $.each(result.result, function() {
                        $('#bodyDasarianTiga').append('<td>' + this['dasarian_3'] + '</td>');
                        dasarian3.push(this['dasarian_3']);
                    });
                }
            );

            $.getJSON(
                'dasarian_data_total/' + tahun + '/' + sawah,
                function(result) {
                    $('#bodyDasarianTotal').empty();
                    $('#bodyDasarianTotal').append('<td><b>Total</b></td>');
                    $.each(result.result, function() {
                        $('#bodyDasarianTotal').append('<td><b>' + this['total'] + '</b></td>');
                    });
                }
            );
            $.getJSON(
                'grafik_ch_bulan/' + tahun + '/' + sawah,
                function(result) {
                    $('#grafik_ch_bulanan').empty();
                    $.each(result.result, function() {
                        var bulan = this['bulan'];
                        var bulan_name = this['bulan_name'];
                        $('#grafik_ch_bulanan').append('<div class="col-lg-6"><center><b>Grafik Curah Hujan Bulan ' + bulan_name + '</b></center></<center><canvas id="grafik_ch_' + this['bulan'] + '" style="width: 50%; height:130px;"></canvas>');
                        $.getJSON(
                            'grafik_ch_tanggal/' + tahun + '/' + sawah + '/' + bulan,
                            function(result) {
                                var tanggal = [];
                                var jumlah_ch = [];
                                $.each(result.result, function() {
                                    tanggal.push(this['tanggal']);
                                    jumlah_ch.push(this['jumlah_ch']);
                                });

                                var ctx = document.getElementById('grafik_ch_' + bulan).getContext('2d');
                                var chart = new Chart(ctx, {
                                    // The type of chart we want to create
                                    type: 'line',

                                    // The data for our dataset
                                    data: {
                                        labels: tanggal,
                                        datasets: [{
                                            label: 'Grafik Curah Hujan Bulan ' + bulan_name,
                                            backgroundColor: 'rgba(255, 255, 255, 0.0)',
                                            borderColor: 'rgb(85, 77, 255)',
                                            data: jumlah_ch
                                        }]
                                    },

                                    // Configuration options go here
                                    options: {
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    max: 200,
                                                    min: 0,
                                                    stepSize: 50
                                                }
                                            }]
                                        },
                                        legend: {
                                            display: false
                                        },
                                        tooltips: {
                                            callbacks: {
                                                label: function(tooltipItem) {
                                                    return tooltipItem.yLabel;
                                                }
                                            }
                                        }
                                    }
                                });
                            });
                    });

                }
            );

            $.getJSON(
                'grafik_ch_bulan/' + tahun + '/' + sawah,
                function(result) {
                    $('#grafik_ch_tahunan').empty();
                    $('#grafik_ch_tahunan').append('<div class="col-lg-12" style="padding-top: 20px;"><center><b>Grafik Curah Hujan Tahun ' + tahun + '</b></center><canvas id="grafik_ch" style="width: 100%; height:350px;"></canvas>');
                    var bulan = [];
                    $.each(result.result, function() {
                        bulan.push(this['bulan_name']);
                    });

                    var ctx = document.getElementById('grafik_ch').getContext('2d');
                    var chart = new Chart(ctx, {
                        // The type of chart we want to create
                        type: 'bar',

                        // The data for our dataset
                        data: {
                            labels: bulan,
                            datasets: [{
                                label: 'Dasarian 1',
                                backgroundColor: 'rgb(50, 108, 255)',
                                data: dasarian1
                            }, {
                                label: 'Dasarian 2',
                                backgroundColor: 'rgb(255, 165, 59)',
                                data: dasarian2
                            }, {
                                label: 'Dasarian 3',
                                backgroundColor: 'rgb(100, 213, 38)',
                                data: dasarian3
                            }]
                        },

                        // Configuration options go here
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        min: 0,
                                        stepSize: 50
                                    }
                                }]
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    });
                }
            );
        });

        $('#tampilkan_data_box_plot').click(function() {
            $('#st_peng').empty();
            if ($('#id_sawah_box').val() == "") {
                $('#dataBoxPlot').prop('hidden', true);
                alert("Stasiun Pengamatan wajib diisi.");
            } else {
                $('#dataGrafik').prop('hidden', true);
                $('#dataBoxPlot').prop('hidden', false);
            }

            var st_peng = $('#id_sawah_box option:selected').text();
            var sbString = st_peng.substr(0, st_peng.indexOf('-'));
            $('#st_peng').append(sbString);

            var sawah = $('#id_sawah_box').val();
            var janQ1Res = null;
            var febQ1Res = null;
            var marQ1Res = null;
            var aprQ1Res = null;
            var meiQ1Res = null;
            var junQ1Res = null;
            var julQ1Res = null;
            var augQ1Res = null;
            var sepQ1Res = null;
            var oktQ1Res = null;
            var novQ1Res = null;
            var desQ1Res = null;

            var janMedRes = null;
            var febMedRes = null;
            var marMedRes = null;
            var aprMedRes = null;
            var meiMedRes = null;
            var junMedRes = null;
            var julMedRes = null;
            var augMedRes = null;
            var sepMedRes = null;
            var oktMedRes = null;
            var novMedRes = null;
            var desMedRes = null;

            var janQ3Res = null;
            var febQ3Res = null;
            var marQ3Res = null;
            var aprQ3Res = null;
            var meiQ3Res = null;
            var junQ3Res = null;
            var julQ3Res = null;
            var augQ3Res = null;
            var sepQ3Res = null;
            var oktQ3Res = null;
            var novQ3Res = null;
            var desQ3Res = null;

            var janMinRes = null;
            var febMinRes = null;
            var marMinRes = null;
            var aprMinRes = null;
            var meiMinRes = null;
            var junMinRes = null;
            var julMinRes = null;
            var augMinRes = null;
            var sepMinRes = null;
            var oktMinRes = null;
            var novMinRes = null;
            var desMinRes = null;

            var janMaxRes = null;
            var febMaxRes = null;
            var marMaxRes = null;
            var aprMaxRes = null;
            var meiMaxRes = null;
            var junMaxRes = null;
            var julMaxRes = null;
            var augMaxRes = null;
            var sepMaxRes = null;
            var oktMaxRes = null;
            var novMaxRes = null;
            var desMaxRes = null;

            $('#bodyBoxPlot').empty();

            $.getJSON(
                'box_plot_data_table/' + sawah,
                function(result) {
                    var arrJan = [];
                    var arrFeb = [];
                    var arrMar = [];
                    var arrApr = [];
                    var arrMei = [];
                    var arrJun = [];
                    var arrJul = [];
                    var arrAug = [];
                    var arrSep = [];
                    var arrOkt = [];
                    var arrNov = [];
                    var arrDes = [];
                    $.each(result.result, function() {
                        arrJan.push(this['jan']);
                        arrFeb.push(this['feb']);
                        arrMar.push(this['mar']);
                        arrApr.push(this['apr']);
                        arrMei.push(this['mei']);
                        arrJun.push(this['jun']);
                        arrJul.push(this['jul']);
                        arrAug.push(this['aug']);
                        arrSep.push(this['sep']);
                        arrOkt.push(this['okt']);
                        arrNov.push(this['nov']);
                        arrDes.push(this['des']);

                        var jan = null;
                        var feb = null;
                        var mar = null;
                        var apr = null;
                        var mei = null;
                        var jun = null;
                        var jul = null;
                        var aug = null;
                        var sep = null;
                        var okt = null;
                        var nov = null;
                        var des = null;
                        if (this['jan'] != "" && this['jan'] != null) {
                            var jan = this['jan'];
                        } else {
                            var jan = "";
                        }
                        if (this['feb'] != "" && this['feb'] != null) {
                            var feb = this['feb'];
                        } else {
                            var feb = "";
                        }
                        if (this['mar'] != "" && this['mar'] != null) {
                            var mar = this['mar'];
                        } else {
                            var mar = "";
                        }
                        if (this['apr'] != "" && this['apr'] != null) {
                            var apr = this['apr'];
                        } else {
                            var apr = "";
                        }
                        if (this['mei'] != "" && this['mei'] != null) {
                            var mei = this['mei'];
                        } else {
                            var mei = "";
                        }
                        if (this['jun'] != "" && this['jun'] != null) {
                            var jun = this['jun'];
                        } else {
                            var jun = "";
                        }
                        if (this['jul'] != "" && this['jul'] != null) {
                            var jul = this['jul'];
                        } else {
                            var jul = "";
                        }
                        if (this['aug'] != "" && this['aug'] != null) {
                            var aug = this['aug'];
                        } else {
                            var aug = "";
                        }
                        if (this['sep'] != "" && this['sep'] != null) {
                            var sep = this['sep'];
                        } else {
                            var sep = "";
                        }
                        if (this['okt'] != "" && this['okt'] != null) {
                            var okt = this['okt'];
                        } else {
                            var okt = "";
                        }
                        if (this['nov'] != "" && this['nov'] != null) {
                            var nov = this['nov'];
                        } else {
                            var nov = "";
                        }
                        if (this['des'] != "" && this['des'] != null) {
                            var des = this['des'];
                        } else {
                            var des = "";
                        }
                        $('#bodyBoxPlot').append(
                            '<tr><td>' + this['tahun'] + '</td>' +
                            '<td>' + jan + '</td>' +
                            '<td>' + feb + '</td>' +
                            '<td>' + mar + '</td>' +
                            '<td>' + apr + '</td>' +
                            '<td>' + mei + '</td>' +
                            '<td>' + jun + '</td>' +
                            '<td>' + jul + '</td>' +
                            '<td>' + aug + '</td>' +
                            '<td>' + sep + '</td>' +
                            '<td>' + okt + '</td>' +
                            '<td>' + nov + '</td>' +
                            '<td>' + des + '</td></tr>'
                        );
                    });

                    // Q1 Rumus
                    arrJan.sort();
                    var inJanQ1 = (25 / 100) * (arrJan.length - 1);
                    if (Math.floor(inJanQ1) == inJanQ1) {
                        janQ1Res = (arrJan[inJanQ1 - 1] + arrJan[inJanQ1]) / 2;
                    } else {
                        janQ1Res = arrJan[Math.floor(inJanQ1)];
                    }

                    arrFeb.sort();
                    var inFebQ1 = (25 / 100) * (arrFeb.length - 1);
                    if (Math.floor(inFebQ1) == inFebQ1) {
                        febQ1Res = (arrFeb[inFebQ1 - 1] + arrFeb[inFebQ1]) / 2;
                    } else {
                        febQ1Res = arrFeb[Math.floor(inFebQ1)];
                    }

                    arrMar.sort();
                    var inMarQ1 = (25 / 100) * (arrMar.length - 1);
                    if (Math.floor(inMarQ1) == inMarQ1) {
                        marQ1Res = (arrMar[inMarQ1 - 1] + arrMar[inMarQ1]) / 2;
                    } else {
                        marQ1Res = arrMar[Math.floor(inMarQ1)];
                    }

                    arrApr.sort();
                    var inAprQ1 = (25 / 100) * (arrApr.length - 1);
                    if (Math.floor(inAprQ1) == inAprQ1) {
                        aprQ1Res = (arrApr[inAprQ1 - 1] + arrApr[inAprQ1]) / 2;
                    } else {
                        aprQ1Res = arrApr[Math.floor(inAprQ1)];
                    }

                    arrMei.sort();
                    var inMeiQ1 = (25 / 100) * (arrMei.length - 1);
                    if (Math.floor(inMeiQ1) == inMeiQ1) {
                        meiQ1Res = (arrMei[inMeiQ1 - 1] + arrMei[inMeiQ1]) / 2;
                    } else {
                        meiQ1Res = arrMei[Math.floor(inMeiQ1)];
                    }

                    arrJun.sort();
                    var inJunQ1 = (25 / 100) * (arrJun.length - 1);
                    if (Math.floor(inJunQ1) == inJunQ1) {
                        junQ1Res = (arrJun[inJunQ1 - 1] + arrJun[inJunQ1]) / 2;
                    } else {
                        junQ1Res = arrJun[Math.floor(inJunQ1)];
                    }

                    arrJul.sort();
                    var inJulQ1 = (25 / 100) * (arrJul.length - 1);
                    if (Math.floor(inJulQ1) == inJulQ1) {
                        julQ1Res = (arrJul[inJulQ1 - 1] + arrJul[inJulQ1]) / 2;
                    } else {
                        julQ1Res = arrJul[Math.floor(inJulQ1)];
                    }

                    arrAug.sort();
                    var inAugQ1 = (25 / 100) * (arrAug.length - 1);
                    if (Math.floor(inAugQ1) == inAugQ1) {
                        augQ1Res = (arrAug[inAugQ1 - 1] + arrAug[inAugQ1]) / 2;
                    } else {
                        augQ1Res = arrAug[Math.floor(inAugQ1)];
                    }

                    arrSep.sort();
                    var inSepQ1 = (25 / 100) * (arrSep.length - 1);
                    if (Math.floor(inSepQ1) == inSepQ1) {
                        sepQ1Res = (arrSep[inSepQ1 - 1] + arrSep[inSepQ1]) / 2;
                    } else {
                        sepQ1Res = arrSep[Math.floor(inSepQ1)];
                    }

                    arrOkt.sort();
                    var inOktQ1 = (25 / 100) * (arrOkt.length - 1);
                    if (Math.floor(inOktQ1) == inOktQ1) {
                        oktQ1Res = (arrOkt[inOktQ1 - 1] + arrOkt[inOktQ1]) / 2;
                    } else {
                        oktQ1Res = arrOkt[Math.floor(inOktQ1)];
                    }

                    arrNov.sort();
                    var inNovQ1 = (25 / 100) * (arrNov.length - 1);
                    if (Math.floor(inNovQ1) == inNovQ1) {
                        novQ1Res = (arrNov[inNovQ1 - 1] + arrNov[inNovQ1]) / 2;
                    } else {
                        novQ1Res = arrNov[Math.floor(inNovQ1)];
                    }

                    arrDes.sort();
                    var inDesQ1 = (25 / 100) * (arrDes.length - 1);
                    if (Math.floor(inDesQ1) == inDesQ1) {
                        desQ1Res = (arrDes[inDesQ1 - 1] + arrDes[inDesQ1]) / 2;
                    } else {
                        desQ1Res = arrDes[Math.floor(inDesQ1)];
                    }

                    // Q3 Rumus
                    var inJanQ3 = (75 / 100) * (arrJan.length - 1);
                    if (Math.floor(inJanQ3) == inJanQ3) {
                        janQ3Res = (arrJan[inJanQ3 - 1] + arrJan[inJanQ3]) / 2;
                    } else {
                        janQ3Res = arrJan[Math.floor(inJanQ3)];
                    }

                    var inFebQ3 = (75 / 100) * (arrFeb.length - 1);
                    if (Math.floor(inFebQ3) == inFebQ3) {
                        febQ3Res = (arrFeb[inFebQ3 - 1] + arrFeb[inFebQ3]) / 2;
                    } else {
                        febQ3Res = arrFeb[Math.floor(inFebQ3)];
                    }

                    var inMarQ3 = (75 / 100) * (arrMar.length - 1);
                    if (Math.floor(inMarQ3) == inMarQ3) {
                        marQ3Res = (arrMar[inMarQ3 - 1] + arrMar[inMarQ3]) / 2;
                    } else {
                        marQ3Res = arrMar[Math.floor(inMarQ3)];
                    }

                    var inAprQ3 = (75 / 100) * (arrApr.length - 1);
                    if (Math.floor(inAprQ3) == inAprQ3) {
                        aprQ3Res = (arrApr[inAprQ3 - 1] + arrApr[inAprQ3]) / 2;
                    } else {
                        aprQ3Res = arrApr[Math.floor(inAprQ3)];
                    }

                    var inMeiQ3 = (75 / 100) * (arrMei.length - 1);
                    if (Math.floor(inMeiQ3) == inMeiQ3) {
                        meiQ3Res = (arrMei[inMeiQ3 - 1] + arrMei[inMeiQ3]) / 2;
                    } else {
                        meiQ3Res = arrMei[Math.floor(inMeiQ3)];
                    }

                    var inJunQ3 = (75 / 100) * (arrJun.length - 1);
                    if (Math.floor(inJunQ3) == inJunQ3) {
                        junQ3Res = (arrJun[inJunQ3 - 1] + arrJun[inJunQ3]) / 2;
                    } else {
                        junQ3Res = arrJun[Math.floor(inJunQ3)];
                    }

                    var inJulQ3 = (75 / 100) * (arrJul.length - 1);
                    if (Math.floor(inJulQ3) == inJulQ3) {
                        julQ3Res = (arrJul[inJulQ3 - 1] + arrJul[inJulQ3]) / 2;
                    } else {
                        julQ3Res = arrJul[Math.floor(inJulQ3)];
                    }

                    var inAugQ3 = (75 / 100) * (arrAug.length - 1);
                    if (Math.floor(inAugQ3) == inAugQ3) {
                        augQ3Res = (arrAug[inAugQ3 - 1] + arrAug[inAugQ3]) / 2;
                    } else {
                        augQ3Res = arrAug[Math.floor(inAugQ3)];
                    }

                    var inSepQ3 = (75 / 100) * (arrSep.length - 1);
                    if (Math.floor(inSepQ3) == inSepQ3) {
                        sepQ3Res = (arrSep[inSepQ3 - 1] + arrSep[inSepQ3]) / 2;
                    } else {
                        sepQ3Res = arrSep[Math.floor(inSepQ3)];
                    }

                    var inOktQ3 = (75 / 100) * (arrOkt.length - 1);
                    if (Math.floor(inOktQ3) == inOktQ3) {
                        oktQ3Res = (arrOkt[inOktQ3 - 1] + arrOkt[inOktQ3]) / 2;
                    } else {
                        oktQ3Res = arrOkt[Math.floor(inOktQ3)];
                    }

                    var inNovQ3 = (75 / 100) * (arrNov.length - 1);
                    if (Math.floor(inNovQ3) == inNovQ3) {
                        novQ3Res = (arrNov[inNovQ3 - 1] + arrNov[inNovQ3]) / 2;
                    } else {
                        novQ3Res = arrNov[Math.floor(inNovQ3)];
                    }

                    var inDesQ3 = (75 / 100) * (arrDes.length - 1);
                    if (Math.floor(inDesQ3) == inDesQ3) {
                        desQ3Res = (arrDes[inDesQ3 - 1] + arrDes[inDesQ3]) / 2;
                    } else {
                        desQ3Res = arrDes[Math.floor(inDesQ3)];
                    }

                    // Rumus Median
                    var inJanMed = arrJan.length / 2;
                    janMedRes = inJanMed % 1 == 0 ? (arrJan[inJanMed - 1] + arrJan[inJanMed]) / 2 : arrJan[Math.floor(inJanMed)];

                    var inFebMed = arrFeb.length / 2;
                    febMedRes = inFebMed % 1 == 0 ? (arrFeb[inFebMed - 1] + arrFeb[inFebMed]) / 2 : arrFeb[Math.floor(inFebMed)];

                    var inMarMed = arrMar.length / 2;
                    marMedRes = inMarMed % 1 == 0 ? (arrMar[inMarMed - 1] + arrMar[inMarMed]) / 2 : arrMar[Math.floor(inMarMed)];

                    var inAprMed = arrApr.length / 2;
                    aprMedRes = inAprMed % 1 == 0 ? (arrApr[inAprMed - 1] + arrApr[inAprMed]) / 2 : arrApr[Math.floor(inAprMed)];

                    var inMeiMed = arrMei.length / 2;
                    meiMedRes = inMeiMed % 1 == 0 ? (arrMei[inMeiMed - 1] + arrMei[inMeiMed]) / 2 : arrMei[Math.floor(inMeiMed)];

                    var inJunMed = arrJun.length / 2;
                    junMedRes = inJunMed % 1 == 0 ? (arrJun[inJunMed - 1] + arrJun[inJunMed]) / 2 : arrJun[Math.floor(inJunMed)];

                    var inJulMed = arrJul.length / 2;
                    julMedRes = inJulMed % 1 == 0 ? (arrJul[inJulMed - 1] + arrJul[inJulMed]) / 2 : arrJul[Math.floor(inJulMed)];

                    var inAugMed = arrAug.length / 2;
                    augMedRes = inAugMed % 1 == 0 ? (arrAug[inAugMed - 1] + arrAug[inAugMed]) / 2 : arrAug[Math.floor(inAugMed)];

                    var inSepMed = arrSep.length / 2;
                    sepMedRes = inSepMed % 1 == 0 ? (arrSep[inSepMed - 1] + arrSep[inSepMed]) / 2 : arrSep[Math.floor(inSepMed)];

                    var inOktMed = arrOkt.length / 2;
                    oktMedRes = inOktMed % 1 == 0 ? (arrOkt[inOktMed - 1] + arrOkt[inOktMed]) / 2 : arrOkt[Math.floor(inOktMed)];

                    var inNovMed = arrNov.length / 2;
                    novMedRes = inNovMed % 1 == 0 ? (arrNov[inNovMed - 1] + arrNov[inNovMed]) / 2 : arrNov[Math.floor(inNovMed)];

                    var inDesMed = arrDes.length / 2;
                    desMedRes = inDesMed % 1 == 0 ? (arrDes[inDesMed - 1] + arrDes[inDesMed]) / 2 : arrDes[Math.floor(inDesMed)];

                    // Rumus Min
                    janMinRes = Math.min.apply(Math, arrJan);
                    febMinRes = Math.min.apply(Math, arrFeb);
                    marMinRes = Math.min.apply(Math, arrMar);
                    aprMinRes = Math.min.apply(Math, arrApr);
                    meiMinRes = Math.min.apply(Math, arrMei);
                    junMinRes = Math.min.apply(Math, arrJun);
                    julMinRes = Math.min.apply(Math, arrJul);
                    augMinRes = Math.min.apply(Math, arrAug);
                    sepMinRes = Math.min.apply(Math, arrSep);
                    oktMinRes = Math.min.apply(Math, arrOkt);
                    novMinRes = Math.min.apply(Math, arrNov);
                    desMinRes = Math.min.apply(Math, arrDes);

                    // Rumus Max
                    janMaxRes = Math.max.apply(Math, arrJan);
                    febMaxRes = Math.max.apply(Math, arrFeb);
                    marMaxRes = Math.max.apply(Math, arrMar);
                    aprMaxRes = Math.max.apply(Math, arrApr);
                    meiMaxRes = Math.max.apply(Math, arrMei);
                    junMaxRes = Math.max.apply(Math, arrJun);
                    julMaxRes = Math.max.apply(Math, arrJul);
                    augMaxRes = Math.max.apply(Math, arrAug);
                    sepMaxRes = Math.max.apply(Math, arrSep);
                    oktMaxRes = Math.max.apply(Math, arrOkt);
                    novMaxRes = Math.max.apply(Math, arrNov);
                    desMaxRes = Math.max.apply(Math, arrDes);

                    var chart = new CanvasJS.Chart("chartContainer", {
                        theme: "light2",
                        axisY: {
                            title: "Curah Hujan (mm)"
                        },
                        data: [{
                            type: "boxAndWhisker",
                            color: "#3C4D2D",
                            upperBoxColor: "#AEF35A",
                            lowerBoxColor: "#B8B2FB",
                            yValueFormatString: "##0.###",
                            // [ Min, Q1, Q3, Max, Median]
                            dataPoints: [{
                                "label": "Jan",
                                "y": [parseFloat(janMinRes), parseFloat(janQ1Res), parseFloat(janQ3Res), parseFloat(janMaxRes), parseFloat(janMedRes)]
                            }, {
                                "label": "Feb",
                                "y": [parseFloat(febMinRes), parseFloat(febQ1Res), parseFloat(febQ3Res), parseFloat(febMaxRes), parseFloat(febMedRes)]
                            }, {
                                "label": "Mar",
                                "y": [parseFloat(marMinRes), parseFloat(marQ1Res), parseFloat(marQ3Res), parseFloat(marMaxRes), parseFloat(marMedRes)]
                            }, {
                                "label": "Apr",
                                "y": [parseFloat(aprMinRes), parseFloat(aprQ1Res), parseFloat(aprQ3Res), parseFloat(aprMaxRes), parseFloat(aprMedRes)]
                            }, {
                                "label": "Mei",
                                "y": [parseFloat(meiMinRes), parseFloat(meiQ1Res), parseFloat(meiQ3Res), parseFloat(meiMaxRes), parseFloat(meiMedRes)]
                            }, {
                                "label": "Jun",
                                "y": [parseFloat(junMinRes), parseFloat(junQ1Res), parseFloat(junQ3Res), parseFloat(junMaxRes), parseFloat(junMedRes)]
                            }, {
                                "label": "Jul",
                                "y": [parseFloat(julMinRes), parseFloat(julQ1Res), parseFloat(julQ3Res), parseFloat(julMaxRes), parseFloat(julMedRes)]
                            }, {
                                "label": "Aug",
                                "y": [parseFloat(augMinRes), parseFloat(augQ1Res), parseFloat(augQ3Res), parseFloat(augMaxRes), parseFloat(augMedRes)]
                            }, {
                                "label": "Sep",
                                "y": [parseFloat(sepMinRes), parseFloat(sepQ1Res), parseFloat(sepQ3Res), parseFloat(sepMaxRes), parseFloat(sepMedRes)]
                            }, {
                                "label": "Okt",
                                "y": [parseFloat(oktMinRes), parseFloat(oktQ1Res), parseFloat(oktQ3Res), parseFloat(oktMaxRes), parseFloat(oktMedRes)]
                            }, {
                                "label": "Nov",
                                "y": [parseFloat(novMinRes), parseFloat(novQ1Res), parseFloat(novQ3Res), parseFloat(novMaxRes), parseFloat(novMedRes)]
                            }, {
                                "label": "Des",
                                "y": [parseFloat(desMinRes), parseFloat(desQ1Res), parseFloat(desQ3Res), parseFloat(desMaxRes), parseFloat(desMedRes)]
                            }]
                        }]
                    });
                    chart.render();

                }
            );
        });
    })(jQuery);
</script>

</Body>

</html>