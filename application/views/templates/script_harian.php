<script>
    var save_method;

    (function($) {

        $('#tgl_pestisida').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#tgl_pemupukan').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#m_edit_tgl_pes').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#m_edit_tgl_pem').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#h_tanggal_edit').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#h_jam_edit').timepicker({
            showMeridian: false
        });
        $('#m_tanggal').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#m_tanggal_edit').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#a_tanggal').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#a_tanggal_edit').datepicker({
            format: 'yyyy-mm-dd'
        });

        var dataHarian = $('#dataHarian').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('admin/get_data_harian') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });

        $('#dataMusiman').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('admin/get_data_musiman') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });

        var dataAkhirMusim = $('#dataAkhirMusim').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('admin/get_data_akhir_musim') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });

        $('#menu_title').text('Data Harian');
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            var target = $(e.target).attr("href") // activated tab
            if (target == "#harian") {
                $('#menu_title').text('Data Harian');
            } else if (target == "#musiman") {
                $('#menu_title').text('Data Musiman');
            } else if (target == "#akhirmusim") {
                $('#menu_title').text('Data Akhir Musiman');
            }
        });

        $('.mdialog-error').fadeTo(2000, 500).slideUp(500, function() {
            $('.mdialog-error').slideUp(500);
        });

        $('.mdialog-success').fadeTo(2000, 500).slideUp(500, function() {
            $('.mdialog-success').slideUp(500);
        });

        var pes = 1;
        $('#btn-add-pestisida').click(function() {
            pes++;
            $('#dynamic_field_pestisida').append('<div id="row_pestisida_' + pes + '"><div class="col-sm-6 mb-1 mb-sm-0">' +
                '<div class="input-group">' +
                '<input type="text" class="form-control" id="tgl_pestisida_' + pes + '" name="tgl_pestisida[]" required />' +
                '<span style="cursor:pointer;" class="input-group-addon entypo-calendar"></span></div></div>' +
                '<div class="col-sm-6">' +
                '<div class="input-group">' +
                '<input class="form-control" name="pestisida_ke[]" id="pestisida_ke[]" type="text" required />' +
                '<span class="input-group-btn">' +
                '<button class="btn btn-danger btn_remove_pestisida" id="' + pes + '" type="button">' +
                '<span class="glyphicon glyphicon-remove"></span></button> </span>' +
                '</div></div></div>');

            $(document).on('click', '.btn_remove_pestisida', function() {
                var button_id = $(this).attr("id");
                $('#row_pestisida_' + button_id + '').remove();
            });

            $('#tgl_pestisida_' + pes + '').datepicker({
                format: 'yyyy-mm-dd'
            });
        });

        var pem = 1;
        $('#btn-add-pemupukan').click(function() {
            pem++;
            $('#dynamic_field_pemupukan').append('<div id="row_pemupukan_' + pem + '"><div class="col-sm-6 mb-1 mb-sm-0">' +
                '<div class="input-group">' +
                '<input type="text" class="form-control" id="tgl_pemupukan_' + pem + '" name="tgl_pemupukan[]" required />' +
                '<span style="cursor:pointer;" class="input-group-addon entypo-calendar"></span></div></div>' +
                '<div class="col-sm-6">' +
                '<div class="input-group">' +
                '<input class="form-control" name="pemupukan_ke[]" id="pemupukan_ke[]" type="text" required />' +
                '<span class="input-group-btn">' +
                '<button class="btn btn-danger btn_remove_pemupukan" id="' + pem + '" type="button">' +
                '<span class="glyphicon glyphicon-remove"></span></button> </span>' +
                '</div></div></div>');

            $(document).on('click', '.edit_btn_remove_pemupukan', function() {
                var button_id = $(this).attr("id");
                $('#row_pemupukan_' + button_id + '').remove();
            });

            $('#tgl_pemupukan_' + pem + '').datepicker({
                format: 'yyyy-mm-dd'
            });
        });

        var pesEdit = 1;
        $('#btn-edit-pestisida').click(function() {
            pesEdit++;
            $('#dynamic_pes_edit').append('<div id="row_pes_edit_' + pesEdit + '"><div class="col-sm-6 mb-1 mb-sm-0">' +
                '<div class="input-group">' +
                '<input type="text" class="form-control" id="m_edit_tgl_pes_' + pesEdit + '" name="m_edit_tgl_pes[]" required />' +
                '<span style="cursor:pointer;" class="input-group-addon entypo-calendar"></span></div></div>' +
                '<div class="col-sm-6">' +
                '<div class="input-group">' +
                '<input class="form-control" name="m_edit_pes_ke[]" id="m_edit_pes_ke[]" type="text" required />' +
                '<span class="input-group-btn">' +
                '<button class="btn btn-danger edit_btn_remove_pes" id="' + pesEdit + '" type="button">' +
                '<span class="glyphicon glyphicon-remove"></span></button> </span>' +
                '</div></div></div>');

            $(document).on('click', '.edit_btn_remove_pes', function() {
                var button_id = $(this).attr("id");
                $('#row_pes_edit_' + button_id + '').remove();
            });

            $('#m_edit_tgl_pes_' + pesEdit + '').datepicker({
                format: 'yyyy-mm-dd'
            });
        });

        var pemEdit = 1;
        $('#btn-edit-pemupukan').click(function() {
            pemEdit++;
            $('#dynamic_pem_edit').append('<div id="row_pem_edit_' + pemEdit + '"><div class="col-sm-6 mb-1 mb-sm-0">' +
                '<div class="input-group">' +
                '<input type="text" class="form-control" id="m_edit_tgl_pem_' + pemEdit + '" name="m_edit_tgl_pem[]" required />' +
                '<span style="cursor:pointer;" class="input-group-addon entypo-calendar"></span></div></div>' +
                '<div class="col-sm-6">' +
                '<div class="input-group">' +
                '<input class="form-control" name="m_edit_pem_ke[]" id="m_edit_pem_ke[]" type="text" required />' +
                '<span class="input-group-btn">' +
                '<button class="btn btn-danger edit_btn_remove_pem" id="' + pemEdit + '" type="button">' +
                '<span class="glyphicon glyphicon-remove"></span></button> </span>' +
                '</div></div></div>');

            $(document).on('click', '.edit_btn_remove_pem', function() {
                var button_id = $(this).attr("id");
                $('#row_pem_edit_' + button_id + '').remove();
            });

            $('#m_edit_tgl_pem_' + pemEdit + '').datepicker({
                format: 'yyyy-mm-dd'
            });
        });


    })(jQuery);

    function edit_harian(id) {
        $('#form_harian').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_harian') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('[name="h_id_harian_edit"]').val(data.id);
                // $('[name="kd_stasiun"]').val(data.kd_stasiun);
                $('[name="h_tanggal_edit"]').val(data.tanggal);
                $('[name="h_jam_edit"]').val(data.jam);
                $('[name="h_jumlah_ch_edit"]').val(data.jumlah_ch);
                $('[name="h_sifat_hujan_edit"]').val(data.sifat_hujan);
                $('[name="h_dampak_hujan_edit"]').val(data.dampak_hujan);
                $('select[name="h_id_stasiun_edit"]').val(data.id_stasiun);
                $('[name="h_tinggi_air_edit"]').val(data.tinggi_air_lahan);
                $('[name="h_hss_edit"]').val(data.hss);
                $('[name="h_hst_edit"]').val(data.hst);
                $('.selectpicker').selectpicker('refresh');
                $('#edit_modal_harian').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Ubah Data Harian'); // Set title to Bootstrap modal title

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function view_harian(id) {
        $('#form_musiman').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_harian') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#id_view').text(data.id);
                // $('[name="kd_stasiun"]').val(data.kd_stasiun);
                $('#tanggal_view').text(data.tanggal);
                $('#jam_view').text(data.jam);
                $('#jumlah_ch_view').text(data.jumlah_ch);
                $('#sifat_hujan_view').text(data.sifat_hujan);
                $('#dampak_hujan_view').text(data.dampak_hujan);
                $('#kd_stasiun_view').text(data.kd_stasiun);
                $('#tinggi_air_view').text(data.tinggi_air_lahan);
                $('#hss_view').text(data.hss);
                $('#hst_view').text(data.hst);
                $('#blok_sawah_view').text(data.blok_sawah);
                $('#pengamat_view').text(data.first_name + ' ' + data.last_name);
                $('#view_modal_harian').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('View Data Harian'); // Set title to Bootstrap modal title

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_harian(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/delete_harian/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataHarian').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }

    function edit_musiman(id) {
        $('#form_harian').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error 
        $('#m_pes_edit').empty();
        $('#m_pem_edit').empty();
        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_musiman') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('[name="m_id_musiman_edit"]').val(data.id);
                $('select[name="m_id_stasiun_edit"]').val(data.id_stasiun);
                $('[name="m_tanggal_edit"]').val(data.tanggal);
                $('[name="m_sumber_air_edit"]').val(data.sumber_air);
                $('[name="m_pengelolaan_air_edit"]').val(data.pengelolaan_air);
                $('[name="m_jenis_tanaman_edit"]').val(data.jenis_tanaman);
                $('[name="m_varietas_tanaman_edit"]').val(data.varietas_tanaman);
                // Pestisida
                $('[name="m_jenis_pestisida_edit"]').val(data.jenis_pestisida);
                $('[name="m_dosis_pestisida_edit"]').val(data.dosis_pestisida);
                $('[name="m_tujuan_pestisida_edit"]').val(data.tujuan_pestisida);
                // Pemupukan
                $('[name="m_jenis_pupuk_edit"]').val(data.jenis_pupuk);
                $('[name="m_dosis_pupuk_edit"]').val(data.dosis_pupuk);
                $('[name="m_tujuan_pemupukan_edit"]').val(data.tujuan_pupuk);
                $('[name="m_jenis_hm_edit"]').val(data.jenis_hm);
                $('[name="m_int_serangan_hm_edit"]').val(data.int_serangan_hm);
                $('[name="m_lama_serangan_hm_edit"]').val(data.lama_serangan_hm);
                $('[name="m_dampak_hm_edit"]').val(data.dampak_tanaman_hm);
                $('[name="m_jenis_pyt_edit"]').val(data.jenis_pyt);
                $('[name="m_int_serangan_pyt_edit"]').val(data.int_serangan_pyt);
                $('[name="m_lama_serangan_pyt_edit"]').val(data.lama_serangan_pyt);
                $('[name="m_dampak_pyt_edit"]').val(data.dampak_tanaman_pyt);
                $('.selectpicker').selectpicker('refresh');
                $('#edit_modal_musiman').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Ubah Data Musiman'); // Set title to Bootstrap modal titl
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });

        $.ajax({
            url: "<?php echo site_url('admin/pestisida_data') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $.each(data, function(index, item) {
                    $('#dynamic_pes_edit').append('<div id="m_pes_edit"><div id="row_pes_edit_' + item.id + '"><div class="col-sm-6 mb-1 mb-sm-0">' +
                        '<div class="input-group">' +
                        '<input type="text" class="form-control" id="m_edit_tgl_pes_' + item.id + '" name="m_edit_tgl_pes[]" value="' + item.tanggal + '" required disabled />' +
                        '<span style="cursor:pointer;" class="input-group-addon entypo-calendar"></span></div></div>' +
                        '<div class="col-sm-6">' +
                        '<div class="input-group">' +
                        '<input class="form-control" name="m_edit_pes_ke[]" id="m_edit_pes_ke[]" type="text" value="' + item.pestisida_ke + '" required disabled />' +
                        '<span class="input-group-btn">' +
                        '<button class="btn btn-danger edit_btn_remove_pes_' + item.id + '" id="' + item.id + '" type="button">' +
                        '<span class="glyphicon glyphicon-remove"></span></button> </span>' +
                        '</div></div></div></div>');

                    $(document).on('click', '.edit_btn_remove_pes_' + item.id + '', function() {
                        var button_id = $(this).attr("id");
                        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
                            $.ajax({
                                type: "POST",
                                url: "<?= site_url('admin/delete_pestisida/'); ?>" + item.id,
                                data: {
                                    id: item.id
                                },
                                success: function(data) {
                                    $('#row_pes_edit_' + button_id + '').remove();
                                }
                            });
                        } else {
                            return false;
                        }
                    });

                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });

        $.ajax({
            url: "<?php echo site_url('admin/pemupukan_data') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $.each(data, function(index, item) {
                    $('#dynamic_pem_edit').append('<div id="m_pem_edit"><div id="row_pem_edit_' + item.id + '"><div class="col-sm-6 mb-1 mb-sm-0">' +
                        '<div class="input-group">' +
                        '<input type="text" class="form-control" id="m_edit_tgl_pem_' + item.id + '" name="m_edit_tgl_pem[]" value="' + item.tanggal + '" required disabled />' +
                        '<span style="cursor:pointer;" class="input-group-addon entypo-calendar"></span></div></div>' +
                        '<div class="col-sm-6">' +
                        '<div class="input-group">' +
                        '<input class="form-control" name="m_edit_pem_ke[]" id="m_edit_pem_ke[]" type="text" value="' + item.pemupukan_ke + '" required disabled />' +
                        '<span class="input-group-btn">' +
                        '<button class="btn btn-danger edit_btn_remove_pem_' + item.id + '" id="' + item.id + '" type="button">' +
                        '<span class="glyphicon glyphicon-remove"></span></button> </span>' +
                        '</div></div></div></div>');

                    $(document).on('click', '.edit_btn_remove_pem_' + item.id + '', function() {
                        var button_id = $(this).attr("id");
                        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
                            $.ajax({
                                type: "POST",
                                url: "<?= site_url('admin/delete_pupuk/'); ?>" + item.id,
                                data: {
                                    id: item.id
                                },
                                success: function(data) {
                                    $('#row_pem_edit_' + button_id + '').remove();
                                }
                            });
                        } else {
                            return false;
                        }
                    });
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function view_musiman(id) {
        $('#form_musiman').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_musiman') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#m_id_view').text(data.id);
                // $('[name="kd_stasiun"]').val(data.kd_stasiun);
                $('#m_kd_stasiun_view').text(data.kd_stasiun);
                $('#m_tanggal_view').text(data.tanggal);
                $('#m_first_name_view').text(data.first_name);
                $('#m_last_name_view').text(data.last_name);
                $('#m_sumber_air_view').text(data.sumber_air);
                $('#m_pengelolaan_air_view').text(data.pengelolaan_air);
                $('#m_jenis_tanaman_view').text(data.jenis_tanaman);
                $('#m_varietas_tanaman_view').text(data.varietas_tanaman);
                $('#m_janis_pestisida_view').text(data.jenis_pestisida);
                $('#m_dosis_pestisida_view').text(data.dosis_pestisida);
                $('#m_tujuan_pestisida_view').text(data.tujuan_pestisida);
                $('#m_jenis_pupuk_view').text(data.jenis_pupuk);
                $('#m_dosis_pupuk_view').text(data.dosis_pupuk);
                $('#m_tujuan_pupuk_view').text(data.tujuan_pupuk);
                $('#m_jenis_hm_view').text(data.jenis_hm);
                $('#m_int_serangan_hm_view').text(data.int_serangan_hm);
                $('#m_lama_serangan_hm_view').text(data.lama_serangan_hm);
                $('#m_dampak_tanaman_hm_view').text(data.dampak_tanaman_hm);
                $('#m_jenis_pyt_view').text(data.jenis_pyt);
                $('#m_int_serangan_pyt_view').text(data.int_serangan_pyt);
                $('#m_lama_serangan_pyt_view').text(data.lama_serangan_pyt);
                $('#m_dampak_tanaman_pyt_view').text(data.dampak_tanaman_pyt);
                $('#view_modal_musiman').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('View Data Musiman'); // Set title to Bootstrap modal title

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });

        $.ajax({
            url: "<?php echo site_url('admin/pestisida_data') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $.each(data, function(index, item) {
                    $('#m_pestisida_view').append('<tr><td style="width: 50%"><b>Tanggal</b></td><td style="width: 50%">' + item.tanggal + '</td></tr><tr><td style="width: 50%"><b>Pestisida Ke</b></td><td style="width: 50%">' + item.pestisida_ke + '</td></tr>');
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });

        $.ajax({
            url: "<?php echo site_url('admin/pemupukan_data') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $.each(data, function(index, item) {
                    $('#m_pemupukan_view').append('<tr><td style="width: 50%"><b>Tanggal</b></td><td style="width: 50%">' + item.tanggal + '</td></tr><tr><td style="width: 50%"><b>Pemupukan Ke</b></td><td style="width: 50%">' + item.pemupukan_ke + '</td></tr>');
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_musiman(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/delete_musiman/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataMusiman').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }

    function edit_akhir_musim(id) {
        $('#form_akhir_musim').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_akhir_musim') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('[name="a_id_amusim_edit"]').val(data.id);
                $('select[name="a_id_stasiun_amusim_edit"]').val(data.id_stasiun);
                $('[name="a_tanggal_edit"]').val(data.tanggal);
                $('[name="a_hasi_panen_edit"]').val(data.hasil_panen_kotor);
                $('select[name="a_cara_panen_edit"]').val(data.cara_panen);
                $('.selectpicker').selectpicker('refresh');
                $('#edit_modal_amusim').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Ubah Data Akhir Musim'); // Set title to Bootstrap modal title

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function view_akhir_musim(id) {
        $('#form_akhir_musim').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('admin/edit_data_akhir_musim') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#a_id_amusim_view').text(data.id);
                $('#a_kd_stasiun_view').text(data.kd_stasiun);
                $('#a_tanggal_view').text(data.tanggal);
                $('#a_pengamat_view').text(data.first_name + ' ' + data.last_name);
                $('#a_hasil_panen_view').text(data.hasil_panen_kotor);
                $('#a_cara_panen_view').text(data.cara_panen);
                $('#view_modal_amusim').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('View Data Akhir Musim'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_akhir_musim(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/delete_akhir_musim/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataAkhirMusim').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>

</Body>

</html>