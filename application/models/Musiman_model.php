<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Musiman_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    // start datatables
    var $table = 'cms_musiman';
    var $column_order = array(null, 'b.kd_stasiun', 'a.sumber_air', 'a.pengelolaan_air', 'a.jenis_tanaman', 'a.varietas_tanaman', 'c.first_name', 'c.last_name');
    var $column_search = array('b.kd_stasiun', 'b.blok_sawah'); //set column field database for datatable searchable
    var $order = array('a.id' => 'desc'); // default order

    private function _get_datatables_query()
    {
        $id = $this->session->userdata('id');
        $id_kabkota = $this->session->userdata('id_kabkota');
        $id_korwil = $this->session->userdata('id_korwil');
        if ($this->session->userdata('role_id') == 1) {
            $this->db->select('a.*, b.kd_stasiun, b.blok_sawah, c.first_name, c.last_name');
            $this->db->from('cms_musiman as a');
            $this->db->join('cms_st_pengamatan as b', 'b.id = a.id_stasiun');
            $this->db->join('cms_user as c', 'c.id = a.id_user');
        } else if ($this->session->userdata('role_id') == 2) {
            $this->db->select('a.*, b.kd_stasiun, b.blok_sawah, c.first_name, c.last_name');
            $this->db->from('cms_musiman as a');
            $this->db->join('cms_st_pengamatan as b', 'b.id = a.id_stasiun');
            $this->db->join('cms_user as c', 'c.id = a.id_user');
        } else if ($this->session->userdata('role_id') == 3) {
            $this->db->select('a.*, b.kd_stasiun, b.blok_sawah, c.first_name, c.last_name');
            $this->db->from('cms_musiman as a');
            $this->db->join('cms_st_pengamatan as b', 'b.id = a.id_stasiun');
            $this->db->join('cms_user as c', 'c.id = a.id_user');
            $this->db->where('c.id_kabkota', $id_kabkota);
        } else if ($this->session->userdata('role_id') == 4) {
            $this->db->select('a.*, b.kd_stasiun, b.blok_sawah, c.first_name, c.last_name');
            $this->db->from('cms_musiman as a');
            $this->db->join('cms_st_pengamatan as b', 'b.id = a.id_stasiun');
            $this->db->join('cms_user as c', 'c.id = a.id_user');
            $this->db->where('c.id_korwil', $id_korwil);
        } else if ($this->session->userdata('role_id') == 5) {
            $this->db->select('a.*, b.kd_stasiun, b.blok_sawah, c.first_name, c.last_name');
            $this->db->from('cms_musiman as a');
            $this->db->join('cms_st_pengamatan as b', 'b.id = a.id_stasiun');
            $this->db->join('cms_user as c', 'c.id = a.id_user');
            $this->db->where('c.id', $id);
        }
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->select('a.*, b.kd_stasiun, b.blok_sawah, c.first_name, c.last_name');
        $this->db->from('cms_musiman as a');
        $this->db->join('cms_st_pengamatan as b', 'b.id = a.id_stasiun');
        $this->db->join('cms_user as c', 'c.id = a.id_user');
        $this->db->where('a.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function pestisida_data($id)
    {
        $this->db->select('*');
        $this->db->from('cms_pestisida');
        $this->db->where('id_musiman', $id);
        $query = $this->db->get()->result_array();

        return $query;
    }

    public function pemupukan_data($id)
    {
        $this->db->select('*');
        $this->db->from('cms_pupuk');
        $this->db->where('id_musiman', $id);
        $query = $this->db->get()->result_array();

        return $query;
    }
}
