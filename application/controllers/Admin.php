<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('view_users_model');
        $this->load->model('harian_model', 'harian');
        $this->load->model('musiman_model', 'musiman');
        $this->load->model('akhir_musim_model', 'akhir_musim');
        $this->load->library('session');
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
        is_logged_in();
    }

    public function dashboard()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['harian'] = $this->view_users_model->total_data_harian()->result_array();
        $data['st_pengamatan'] = $this->view_users_model->total_st_pengamatan()->result_array();
        $data['petani'] = $this->view_users_model->total_data_petani()->result_array();
        $data['data_harian'] = $this->view_users_model->last_data_harian()->result_array();
        $data['menu_title'] = "";
        $data['url'] = "admin/dashboard";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('admin/dashboard');
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_dashboard');
    }

    public function grafik_petani_data()
    {
        $data = $this->view_users_model->grafik_petani_data()->result_array();
        echo json_encode(array('result' => $data));
    }

    public function stasiun_pengamatan()
    {
        $this->form_validation->set_rules('kd_stasiun', 'Kode Stasiun', 'required|trim');
        if ($this->form_validation->run() == false) {
            $config['web'] = $this->view_users_model->config_data()->result_array();
            $data['stasiun'] = $this->view_users_model->st_peng_data()->result_array();
            $data['kabkota'] = $this->view_users_model->kabkota_data();
            $data['pengamat'] = $this->view_users_model->pengamat_data()->result_array();
            $data['role_id'] = $this->session->userdata('role_id');
            $data['id_kabkota'] = $this->session->userdata('id_kabkota');
            $data['id_korwil'] = $this->session->userdata('id_korwil');
            $data['menu_title'] = "Stasiun Pengamatan";
            $data['url'] = "admin/stasiun_pengamatan";
            $this->load->view('templates/header', $config);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/breadcumb', $data);
            $this->load->view('admin/stasiun', $data);
            $this->load->view('templates/footer', $config);
            $this->load->view('templates/script_stasiun');
        } else {
            $data = [
                'kd_stasiun' => $this->input->post('kd_stasiun'),
                'id_desa' => $this->input->post('id_desa'),
                'id_kabkota' => $this->input->post('id_kabkot'),
                'id_wilayah' => $this->input->post('id_wilayah'),
                'blok_sawah' => $this->input->post('blok_sawah'),
                'jenis_tanah' => $this->input->post('jenis_tanah'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'luas' => $this->input->post('luas_tanah'),
                'id_user' => $this->input->post('id_pemilik'),
                'kelompok_tani' => $this->input->post('kelompok_tani')
            ];
            $this->db->insert('cms_st_pengamatan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Penambahan data sawah baru sukses!</div>');
            redirect('admin/stasiun_pengamatan');
        }
    }

    public function desa_data($check)
    {
        $data = $this->view_users_model->desa_data($check);
        echo json_encode(array('result' => $data));
    }

    public function wilayah_data($check)
    {
        $data = $this->view_users_model->wilayah_data($check);
        echo json_encode(array('result' => $data));
    }

    public function edit_stasiun($id)
    {
        $where = ['id' => $id];
        $data = [
            'kd_stasiun' => $this->input->post('kd_stasiun'),
            'id_desa' => $this->input->post('id_desa_' . $id),
            'id_wilayah' => $this->input->post('id_wilayah_' . $id),
            'blok_sawah' => $this->input->post('blok_sawah'),
            'jenis_tanah' => $this->input->post('jenis_tanah'),
            'latitude' => $this->input->post('latitude'),
            'longitude' => $this->input->post('longitude'),
            'luas' => $this->input->post('luas_tanah'),
            'id_user' => $this->input->post('id_pemilik_' . $id),
            'kelompok_tani' => $this->input->post('kelompok_tani')
        ];
        $this->view_users_model->edit_stasiun($where, $data, 'cms_st_pengamatan');
        $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Perubahan data sawah sukses!</div>');
        redirect('admin/stasiun_pengamatan');
    }

    public function delete_stasiun($id)
    {
        $where = array('id' => $id);
        $this->view_users_model->delete_stasiun($where, 'cms_st_pengamatan');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Hapus data stasiun pengamatan sukses!</div>');
        redirect('admin/stasiun_pengamatan');
    }

    public function input_data($input = null)
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['st_pengamatan'] = $this->view_users_model->st_peng_data()->result_array();
        $data['menu_title'] = "Harian";
        $data['url'] = "admin/input_data";
        $data['tab'] = $input;
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('admin/input_data', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_harian');
    }

    public function add_harian()
    {
        $this->form_validation->set_rules('id_stasiun_harian', 'ID Sawah', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger mdialog-error" role="alert">Kode stasiun wajib diisi!</div>');
            redirect('admin/input_data/harian');
        } else {
            $data = [
                'tanggal' => $this->input->post('tanggal'),
                'jam' => $this->input->post('jam'),
                'jumlah_ch' => $this->input->post('jumlah_ch'),
                'tinggi_air_lahan' => $this->input->post('tinggi_air'),
                'hss' => $this->input->post('hss'),
                'hst' => $this->input->post('hst'),
                'sifat_hujan' => $this->input->post('sifat_hujan'),
                'dampak_hujan' => $this->input->post('dampak_hujan'),
                'id_stasiun' => $this->input->post('id_stasiun_harian'),
                'id_user' => $this->session->userdata('id')
            ];

            $this->db->insert('cms_harian', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Penambahan data harian sukses!</div>');
            redirect('admin/input_data/harian');
        }
    }

    public function edit_harian()
    {
        $id = $this->input->post('h_id_harian_edit');
        $where = ['id' => $id];
        $data = [
            'tanggal' => $this->input->post('h_tanggal_edit'),
            'jam' => $this->input->post('h_jam_edit'),
            'jumlah_ch' => $this->input->post('h_jumlah_ch_edit'),
            'tinggi_air_lahan' => $this->input->post('h_tinggi_air_edit'),
            'hss' => $this->input->post('h_hss_edit'),
            'hst' => $this->input->post('h_hst_edit'),
            'sifat_hujan' => $this->input->post('h_sifat_hujan_edit'),
            'dampak_hujan' => $this->input->post('h_dampak_hujan_edit'),
            'id_stasiun' => $this->input->post('h_id_stasiun_edit'),
            'id_user' => $this->session->userdata('id')
        ];

        $this->view_users_model->edit_harian($where, $data, 'cms_harian');
        $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Perubahan data harian sukses!</div>');
        redirect('admin/input_data/harian');
    }

    public function add_musiman()
    {
        $this->form_validation->set_rules('id_stasiun_musiman', 'ID Sawah', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger mdialog-error" role="alert">Kode stasiun wajib diisi!</div>');
            redirect('admin/input_data/musiman');
        } else {
            $data = [
                'id_stasiun' => $this->input->post('id_stasiun_musiman'),
                'tanggal' => $this->input->post('m_tanggal'),
                'sumber_air' => $this->input->post('sumber_air'),
                'pengelolaan_air' => $this->input->post('pengelolaan_air'),
                'jenis_tanaman' => $this->input->post('jenis_tanaman'),
                'varietas_tanaman' => $this->input->post('varietas_tanaman'),
                'jenis_pestisida' => $this->input->post('jenis_pestisida'),
                'dosis_pestisida' => $this->input->post('dosis_pestisida'),
                'tujuan_pestisida' => $this->input->post('tujuan_pestisida'),
                'jenis_pupuk' => $this->input->post('jenis_pupuk'),
                'dosis_pupuk' => $this->input->post('dosis_pupuk'),
                'tujuan_pupuk' => $this->input->post('tujuan_pemupukan'),
                'jenis_hm' => $this->input->post('jenis_hm'),
                'int_serangan_hm' => $this->input->post('int_serangan_hm'),
                'lama_serangan_hm' => $this->input->post('lama_serangan_hm'),
                'dampak_tanaman_hm' => $this->input->post('dampak_hm'),
                'jenis_pyt' => $this->input->post('jenis_pyt'),
                'int_serangan_pyt' => $this->input->post('int_serangan_pyt'),
                'lama_serangan_pyt' => $this->input->post('lama_serangan_pyt'),
                'dampak_tanaman_pyt' => $this->input->post('dampak_pyt'),
                'id_user' => $this->session->userdata('id')
            ];

            $this->db->insert('cms_musiman', $data);
            $id_musiman = $this->db->insert_id();
            $this->add_pestisida($id_musiman);
            $this->add_pupuk($id_musiman);
            $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Penambahan data musiman sukses!</div>');
            redirect('admin/input_data/musiman');
        }
    }

    public function edit_musiman()
    {
        $id = $this->input->post('h_id_musiman_edit');
        $where = ['id' => $id];
        $data = [
            'id_stasiun' => $this->input->post('m_id_stasiun_edit'),
            'tanggal' => $this->input->post('m_tanggal_edit'),
            'sumber_air' => $this->input->post('m_sumber_air_edit'),
            'pengelolaan_air' => $this->input->post('m_pengelolaan_air_edit'),
            'jenis_tanaman' => $this->input->post('m_jenis_tanaman_edit'),
            'varietas_tanaman' => $this->input->post('m_varietas_tanaman_edit'),
            'jenis_pestisida' => $this->input->post('m_jenis_pestisida_edit'),
            'dosis_pestisida' => $this->input->post('m_dosis_pestisida_edit'),
            'tujuan_pestisida' => $this->input->post('m_tujuan_pestisida_edit'),
            'jenis_pupuk' => $this->input->post('m_jenis_pupuk_edit'),
            'dosis_pupuk' => $this->input->post('m_dosis_pupuk_edit'),
            'tujuan_pupuk' => $this->input->post('m_tujuan_pemupukan_edit'),
            'jenis_hm' => $this->input->post('m_jenis_hm_edit'),
            'int_serangan_hm' => $this->input->post('m_int_serangan_hm_edit'),
            'lama_serangan_hm' => $this->input->post('m_lama_serangan_hm_edit'),
            'dampak_tanaman_hm' => $this->input->post('m_dampak_hm_edit'),
            'jenis_pyt' => $this->input->post('m_jenis_pyt_edit'),
            'int_serangan_pyt' => $this->input->post('m_int_serangan_pyt_edit'),
            'lama_serangan_pyt' => $this->input->post('m_lama_serangan_pyt_edit'),
            'dampak_tanaman_pyt' => $this->input->post('m_dampak_pyt_edit'),
            'id_user' => $this->session->userdata('id')
        ];

        $this->view_users_model->edit_musiman($where, $data, 'cms_musiman');
        $this->edit_pestisida($id);
        $this->edit_pupuk($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Perubahan data musiman sukses!</div>');
        redirect('admin/input_data/musiman');
    }

    public function add_pestisida($id_musiman)
    {
        $tanggal = $this->input->post('tgl_pestisida');
        $pestisida = $this->input->post('pestisida_ke');
        if (!empty($tanggal) && !empty($pestisida)) {
            foreach ($tanggal as $key => $value) {
                $data['id_musiman'] = $id_musiman;
                $data['tanggal'] = $value;
                $data['pestisida_ke'] = $pestisida[$key];
                $this->db->insert('cms_pestisida', $data);
            }
        }
    }

    public function edit_pestisida($id)
    {
        $tanggal = $this->input->post('m_edit_tgl_pes');
        $pestisida = $this->input->post('m_edit_pes_ke');
        if (!empty($tanggal) && !empty($pestisida)) {
            foreach ($tanggal as $key => $value) {
                $data['id_musiman'] = $id;
                $data['tanggal'] = $value;
                $data['pestisida_ke'] = $pestisida[$key];
                $this->db->insert('cms_pestisida', $data);
            }
        }
    }

    public function delete_pestisida($id)
    {
        $this->view_users_model->delete_pestisida($id);
        echo 'Data pestisida berhasil dihapus!';
    }

    public function add_pupuk($id_musiman)
    {
        $tanggal = $this->input->post('tgl_pemupukan');
        $pupuk = $this->input->post('pemupukan_ke');
        if (!empty($tanggal) && !empty($pupuk)) {
            foreach ($tanggal as $key => $value) {
                $data['id_musiman'] = $id_musiman;
                $data['tanggal'] = $value;
                $data['pemupukan_ke'] = $pupuk[$key];
                $this->db->insert('cms_pupuk', $data);
            }
        }
    }

    public function edit_pupuk($id)
    {
        $tanggal = $this->input->post('m_edit_tgl_pem');
        $pupuk = $this->input->post('m_edit_pem_ke');
        if (!empty($tanggal) && !empty($pupuk)) {
            foreach ($tanggal as $key => $value) {
                $data['id_musiman'] = $id;
                $data['tanggal'] = $value;
                $data['pemupukan_ke'] = $pupuk[$key];
                $this->db->insert('cms_pupuk', $data);
            }
        }
    }

    public function delete_pupuk($id)
    {
        $this->view_users_model->delete_pupuk($id);
        echo 'Data pemupukan berhasil dihapus!';
    }

    public function add_akhir_musim()
    {
        $this->form_validation->set_rules('id_stasiun_amusim', 'ID Sawah', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger mdialog-error" role="alert">Kode stasiun wajib diisi!</div>');
            redirect('admin/input_data/akhirmusim');
        } else {
            $data = [
                'id_stasiun' => $this->input->post('id_stasiun_amusim'),
                'tanggal' => $this->input->post('a_tanggal'),
                'id_user' => $this->session->userdata('id'),
                'hasil_panen_kotor' => $this->input->post('hasil_panen'),
                'cara_panen' => $this->input->post('cara_panen')
            ];

            $this->db->insert('cms_akhir_musim', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Penambahan data akhir musim sukses!</div>');
            redirect('admin/input_data/akhirmusim');
        }
    }

    public function edit_akhir_musim()
    {
        $id = $this->input->post('a_id_amusim_edit');
        $where = ['id' => $id];
        $data = [
            'id_stasiun' => $this->input->post('a_id_stasiun_amusim_edit'),
            'tanggal' => $this->input->post('a_tanggal_edit'),
            'id_user' => $this->session->userdata('id'),
            'hasil_panen_kotor' => $this->input->post('a_hasi_panen_edit'),
            'cara_panen' => $this->input->post('a_cara_panen_edit')
        ];

        $this->view_users_model->edit_akhir_musim($where, $data, 'cms_akhir_musim');
        $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Perubahan data akhir musim sukses!</div>');
        redirect('admin/input_data/akhirmusim');
    }

    public function grafik()
    {
        $config['web'] = $this->view_users_model->config_data()->result_array();
        $data['stasiun'] = $this->view_users_model->st_peng_data()->result_array();
        $data['menu_title'] = "Grafik";
        $data['url'] = "admin/grafik";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('admin/grafik', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_grafik');
    }

    public function tahun_data($check)
    {
        $data = $this->view_users_model->tahun_data($check);
        echo json_encode(array('result' => $data));
    }

    public function dasarian_data_satu($tahun, $sawah)
    {
        $data = $this->view_users_model->dasarian_data_satu($tahun, $sawah);
        echo json_encode(array('result' => $data));
    }

    public function dasarian_data_dua($tahun, $sawah)
    {
        $data = $this->view_users_model->dasarian_data_dua($tahun, $sawah);
        echo json_encode(array('result' => $data));
    }

    public function dasarian_data_tiga($tahun, $sawah)
    {
        $data = $this->view_users_model->dasarian_data_tiga($tahun, $sawah);
        echo json_encode(array('result' => $data));
    }

    public function dasarian_data_total($tahun, $sawah)
    {
        $data = $this->view_users_model->dasarian_data_total($tahun, $sawah);
        echo json_encode(array('result' => $data));
    }

    public function grafik_ch_bulan($tahun, $sawah)
    {
        $data = $this->view_users_model->grafik_ch_bulan($tahun, $sawah);
        echo json_encode(array('result' => $data));
    }

    public function grafik_ch_tanggal($tahun, $sawah, $bulan)
    {
        $data = $this->view_users_model->grafik_ch_tanggal($tahun, $sawah, $bulan);
        echo json_encode(array('result' => $data));
    }

    public function import_data_harian()
    {
        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/import/";
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('import_harian_file')) {
            //upload gagal
            $this->session->set_flashdata('message', '<div class="alert alert-danger mdialog-danger" role="alert">Import data harian gagal! ' . $this->upload->display_errors() . '</div>');
            //redirect halaman
            redirect('admin/input_data/harian');
        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel       = $excelreader->load($_SERVER['DOCUMENT_ROOT'] . '/assets/uploads/import/' . $data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet           = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

            $data = array();

            $numrow = 1;
            foreach ($sheet as $row) {
                if ($numrow > 1) {
                    array_push($data, array(
                        'tanggal'           => $row['A'],
                        'jam'               => $row['B'],
                        'jumlah_ch'         => $row['C'],
                        'sifat_hujan'       => $row['D'],
                        'dampak_hujan'      => $row['E'],
                        'tinggi_air_lahan'  => $row['F'],
                        'hss'               => $row['G'],
                        'hst'               => $row['H'],
                        'id_stasiun'        => $this->input->post('import_id_stasiun_harian'),
                        'id_user'           => $this->session->userdata('id')
                    ));
                }
                $numrow++;
            }
            $this->db->insert_batch('cms_harian', $data);
            //delete file from server
            unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/uploads/import/' . $data_upload['file_name']);

            //upload success
            $this->session->set_flashdata('message', '<div class="alert alert-success mdialog-success" role="alert">Import data harian sukses!</div>');
            //redirect halaman
            redirect('admin/input_data/harian');
        }
    }

    public function box_plot_data_table($sawah)
    {
        $data = $this->view_users_model->box_data_table($sawah);
        echo json_encode(array('result' => $data));
    }

    public function get_data_harian()
    {
        $list = $this->harian->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->kd_stasiun;
            $row[] = $item->tanggal;
            $row[] = $item->jam;
            $row[] = $item->jumlah_ch;
            $row[] = $item->blok_sawah;
            $row[] = $item->first_name . ' ' . $item->last_name;
            // add html for action
            if ($this->session->userdata('role_id') == 2) {
                $btnDeleteHarian = '<button class="btn-link d-delete" onclick="delete_harian(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button>';
            } else {
                $btnDeleteHarian = "";
            }
            $row[] = '<center><button class="btn-link d-view" onclick="view_harian(' . "'" . $item->id . "'" . ')" title="Lihat"><i class="fontello-eye-outline" style="color:blue; font-size:15px;"></i></button> 
            <button class="btn-link d-edit" onclick="edit_harian(' . "'" . $item->id . "'" . ')" title="Ubah"><i class="fa fa-edit" style="color:green; font-size:13px;"></i></button> ' . $btnDeleteHarian . '</center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->harian->count_all(),
            "recordsFiltered" => $this->harian->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_harian($id)
    {
        $data = $this->harian->get_by_id($id);
        echo json_encode($data);
    }

    public function delete_harian($id)
    {
        $this->view_users_model->delete_harian($id);
        echo 'Data harian berhasil dihapus!';
    }

    public function get_data_musiman()
    {
        $list = $this->musiman->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->kd_stasiun;
            $row[] = $item->tanggal;
            $row[] = $item->sumber_air;
            $row[] = $item->pengelolaan_air;
            $row[] = $item->jenis_tanaman;
            $row[] = $item->varietas_tanaman;
            $row[] = $item->first_name . ' ' . $item->last_name;
            // add html for action
            if ($this->session->userdata('role_id') == 2) {
                $btnDeleteMusiman = '<button class="btn-link d-delete" onclick="delete_musiman(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button>';
            } else {
                $btnDeleteMusiman = "";
            }
            $row[] = '<center><button class="btn-link d-view" onclick="view_musiman(' . "'" . $item->id . "'" . ')" title="Lihat"><i class="fontello-eye-outline" style="color:blue; font-size:15px;"></i></button> 
            <button class="btn-link d-edit" onclick="edit_musiman(' . "'" . $item->id . "'" . ')" title="Ubah"><i class="fa fa-edit" style="color:green; font-size:13px;"></i></button> ' . $btnDeleteMusiman . '</center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->musiman->count_all(),
            "recordsFiltered" => $this->musiman->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function pestisida_data($id)
    {
        $data = $this->musiman->pestisida_data($id);
        echo json_encode($data);
    }

    public function pemupukan_data($id)
    {
        $data = $this->musiman->pemupukan_data($id);
        echo json_encode($data);
    }

    public function edit_data_musiman($id)
    {
        $data = $this->musiman->get_by_id($id);
        echo json_encode($data);
    }

    public function delete_musiman($id)
    {
        $this->view_users_model->delete_musiman($id);
        echo 'Data musiman berhasil dihapus!';
    }

    public function get_data_akhir_musim()
    {
        $list = $this->akhir_musim->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->kd_stasiun;
            $row[] = $item->tanggal;
            $row[] = $item->hasil_panen_kotor;
            $row[] = $item->cara_panen;
            // add html for action
            if ($this->session->userdata('role_id') == 2) {
                $btnDeleteAkhirMusim = '<button class="btn-link d-delete" onclick="delete_akhir_musim(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button>';
            } else {
                $btnDeleteAkhirMusim = "";
            }
            $row[] = '<center><button class="btn-link d-view" onclick="view_akhir_musim(' . "'" . $item->id . "'" . ')" title="Lihat"><i class="fontello-eye-outline" style="color:blue; font-size:15px;"></i></button> 
            <button class="btn-link d-edit" onclick="edit_akhir_musim(' . "'" . $item->id . "'" . ')" title="Ubah"><i class="fa fa-edit" style="color:green; font-size:13px;"></i></button> ' . $btnDeleteAkhirMusim . '</center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->musiman->count_all(),
            "recordsFiltered" => $this->musiman->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_akhir_musim($id)
    {
        $data = $this->akhir_musim->get_by_id($id);
        echo json_encode($data);
    }

    public function delete_akhir_musim($id)
    {
        $this->view_users_model->delete_akhir_musim($id);
        echo 'Data akhir musim berhasil dihapus!';
    }
}
